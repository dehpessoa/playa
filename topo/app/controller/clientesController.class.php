<?php

require_once(model_path."Usuarios.class.php");

class clientesController extends sessionController{

	public function index($args){
		global $user;

		$render = new Render();

		if( $user->id > 0){
			if(count($args['args']) > 1){
				switch ($args['args'][1]) {
					case 'deletar':						
						
						$new = new Cliente();
						$new->load(array('id', $args['args'][0]));

						if( $this->pick('confirm', false) ){
							if($new->delete()){
								$render->json(array('result'=>'success','data'=>'Cliente deletado com sucesso.'));
							}else{
								$render->json(array('result'=>'success','data'=>'Erro ao deletar cliente.'));
							}
						}else{
							$render->json(array('result'=>'confirm','data'=>'Deletar cliente "' . $new->name . '" ?'));
						}

						break;
					case 'atualizar':

							$values = array_filter( $this->pick(
														array(
															'name',
															'slug'
														)
													));

							$newPlayer = new Cliente();
							$newPlayer->id = $this->pick('id',0);
							$newPlayer->load();

							// VALIDATE
							if( $this->pick('slug') &&
								$this->pick('slug') !== $newPlayer->slug &&
								$newPlayer->exists(array('slug',$this->pick('slug'))) 
							){
								$render->json(array('result'=>'error','data'=>'Esse slug já existe.'));
								break;
							}		

							/*
							foreach ($values as $key => $value) {
								$newPlayer->{$key} = $value;
							}*/

							$newPlayer->update($values);
							if( isset($_FILES['background']) ){
								foreach ($_FILES['background']['name'] as $key => $value) {

									$erros = $newPlayer->digestFile(
										array(
											'type' 		=> $_FILES['background']['type'][$key],
											'size' 		=> $_FILES['background']['size'][$key],
											'name'		=> $_FILES['background']['name'][$key],
											'tmp_name' 	=> $_FILES['background']['tmp_name'][$key],
										),
										'uploads/images/' . $_FILES['background']['name'][$key],
										'uploads/images'
									);

									if( count($erros) ){
										array_push( $erros , 'Erro ao processar: ' . $_FILES['background']['name'][$key] );
										$render->json(array('result'=>'error','data'=> implode("<br/>", $erros) ));
										die();
									}
									
								}
							}

							//print_r($newPlayer);

							if( $newPlayer->save() ){
								$render->json(
									array(
										'result'=>'success',
										'data'=> array((object) $newPlayer->attributes)
										)
									);
							}else{
								$render->json(array('result'=>'error','data'=>'Erro ao salvar o cliente.'));
							}

						break;
					default:
						$render->json(array('result'=>'error','data'=>'Não encontrado.'));
						break;
				}
			}elseif(count($args['args']) > 0 && $args['args'][0] == 'criar'){		

				$return = array();
				
				$newPlayer = new Cliente(
						array(
							'name' => $this->pick('name',''),
							'slug' => $this->createSlug($this->pick('name',''))						
						)
					);

				// VALIDATE
				switch ( true ) {
					case ( $newPlayer->name == '' ):
						$render->json(array('result'=>'error','data'=>'O nome é obrigatório.'));
						return;
						break;

					case ( $this->pick('name', false ) && $newPlayer->exists( array('slug',$this->pick('name') ) ) ):
						$render->json(array('result'=>'error','data'=>'Esse slug já existe.'));
						return;
						break;
					
					default:
						# code...
						break;
				}

				if( $newPlayer->create() ){					
					$newPlayer->load();
					//$newPlayer->client = $newPlayer->getClientName();

					$render->json(
						array(
							'result'=>'success',
							'data'=> array((object) $newPlayer->attributes)
							)
						);
				}else{
					$render->json(array('result'=>'error','data'=>'Erro ao criar cliente.'));
				}

			}else{

				$players = new Clientes(new Cliente());
				$filter = array();

				$page 	 = (int)$this->pick('page', 1) - 1;
				$perPage = (int)$this->pick('perPage', 1000);
				$startPage  = intval($page * $perPage);
				$perPage	= intval($perPage);

				if( count($filter) == 0 ){
					$filter[] = array('status','0','>');
					$players->load($filter,array('name'),array( $startPage, $perPage ));
				}else{
					$filter[] = array('status','0','>');
					$players->load($filter,array('name'),array( $startPage, $perPage ));
				}
				
				$ret = array();
				foreach ($players->models as $key => $player) {
					$data = (object)$player->attributes;
					$ret[] = $data;
				}
				$render->json(array('result'=>'success','data'=>$ret));
			}
		}else{
			$render->json(array('result'=>'deny-logout','data'=>'Usuário off-line.'));
		}
		
	
	}	

	
}
?>
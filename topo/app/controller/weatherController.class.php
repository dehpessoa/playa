<?php

require_once(model_path."Weather.class.php");

class weatherController extends Controller{

	public function index($args){
		$render = new Render();

		$url = $this->pick('url',false);

		if( !$url ){ $render->json(array('result'=>'fail','data'=>'The weather is VERY strange...')); return false; }

		$weather = new Weather();

		$curret_weather = $weather->getData( $url );

		$render->json(array('result'=>'success','data'=>$curret_weather));


	}

}

?>
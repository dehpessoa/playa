<?php

require_once(model_path."Rooms.class.php");

class roomsController extends Controller{

	public function index($args){
		$render = new Render();
		$render->json(array('result'=>'success','data'=>'Bem vindo ao exemplo de JSON!'));
	}

	public function roomsList($args){
		$render = new Render();		

		if( count($args['args']) == 1 && $args['args'][0] !== 'all' ){
			$room = new Room();

			if( is_numeric( $args['args'][0] ) ){
				$room->load(array(array("id_casa",$args['args'][0])));
			}else{
				$room->load(array(array("location",$args['args'][0])));				
			}
			
			$render->json(array('result'=>'success','data'=>$room->attributes));
		}else{
			$rooms = new Rooms(new Room());
			$rooms->getAll();

			$ret = array();
			$types = array();
			
			foreach ($rooms->models as $key => $room) {
				//$room->getDayPrice( (count($args['args'])>1)?$args['args'][1]:false );

				$data = (object)$room->attributes;	
				$ret[] = $data;

				$data->slug = preg_replace("/-/", "_", $this->createSlug($data->casa));
				$data->dates = array();
				$data->specials = array();
				$data->days = array();
				$data->types = array();				

				// with start AND end dates
				if( count($args['args']) == 3 ){
					
					$types = $room->loadUnitDaysTypes( $args['args'][1], $args['args'][2] );
					//$data->types = $types;
					if( $types ){
						foreach ($types as $key => $type) {
							$type['date'] =  date('j-n-Y', strtotime($type['date']));
							array_push($data->types, $type);
						}
					}

					$dates = $room->loadUnitDays( $args['args'][1], $args['args'][2] );

					if($dates){
						$lastMinute = false;
						foreach ($dates as $key => $date) {
							$date['date'] =  date('j-n-Y', strtotime($date['date']));
							$date['price'] = (float)$date['price'];

							if( ( time() + (7 * 24 * 60 * 60) ) - strtotime($date['date']) > 0 ){								
								$date['price'] = $date['price'] * 0.8;
								$date['caption'] = 'last minute';
								$date['color_selected'] = "b54e69";
								$date['color_unselected'] = "db5f81";
								$lastMinute = $lastMinute? $lastMinute : $date;
							}

							if( time() - strtotime($date['date']) >= 0 ){
								$date['fecha'] = $date['date'];
								$date['caption'] = 'Past day';
							}

							//array_push($data->specials, $date);
							$data->days[$date['date']] = $date;
						};
					};

					if( $lastMinute ){
						array_push($data->types, $lastMinute);
					}

				}

				// with only start date
				if( count($args['args']) == 2 ){
					
					$types = $room->loadUnitDaysTypes( $args['args'][1], $args['args'][1] );
					//$data->types = $types;
					if( $types ){
						foreach ($types as $key => $type) {
							$type['date'] =  date('j-n-Y', strtotime($type['date']));
							array_push($data->types, $type);
						}
					}

					$dates = $room->loadUnitDays( $args['args'][1], $args['args'][1] );

					if($dates){
						$lastMinute = false;
						foreach ($dates as $key => $date) {
							$date['date'] =  date('j-n-Y', strtotime($date['date']));
							$date['price'] = (float)$date['price'];

							if( ( time() + (7 * 24 * 60 * 60) ) - strtotime($date['date']) > 0 ){
								$date['price'] = $date['price'] * 0.8;
								$date['caption'] = 'Last Minute';
								$date['color_selected'] = "9A3128";
								$date['color_unselected'] = "DD594E";
								$lastMinute = $lastMinute? $lastMinute : $date;
							}

							if( time() - strtotime($date['date']) >= 0 ){
								$date['fecha'] = $date['date'];
								$date['caption'] = 'Past day';
							}

							//array_push($data->specials, $date);
							$data->days[$date['date']] = $date;
						};

						if( $lastMinute ){
							array_push($data->types, $lastMinute);
						}

					};
				}
			
			}
			$render->json(array('result'=>'success','data'=> $ret ));
		}

	}

	public function roomsRender($args){
		
		$diretorioPrincipal = $_SERVER['DOCUMENT_ROOT']."/playa/rooms/json/";

		// Renderiza JSON
		function renderJSON($pathJson, $nameJson = "semnome"){
			$debug = true;
			$render = new Render();

			// Monta objeto com o JSON e envia pro template
			$jsonDecoded = json_decode($pathJson);
			//print_r($jsonDecoded);
			//print_r("<hr>");
			$render->add('quarto', $jsonDecoded);
			$html_content = $render->render('room');

			$fileLocation = $_SERVER['DOCUMENT_ROOT']  . "/playa/rooms/".$nameJson.".html";
		    file_put_contents($fileLocation, $html_content);

		    $link = $_SERVER['HTTP_HOST']  . "/playa/rooms/".$nameJson.".html";

		    if($debug) { print '<font color="red">- - - - CREATE HTML >>>>>>>> <a target="_blank" href="http://'.$link.'">'.$link.'</a></font><br>'; }

		}

		// Verifica se é JSON
		function is_json($input) {
		    $input = trim($input);
		    if (substr($input,0,1)!='{' OR substr($input,-1,1)!='}')
		        return false;
		    return is_array(@json_decode($input, true));
		}

		function readDirectory($path){

			$diretorioPrincipal = $_SERVER['DOCUMENT_ROOT']."/playa/rooms/json/";
			$debug = true;

			$diretorio = dir($path);
			while($arquivo = $diretorio -> read()){
				$caminhoArquivo = $arquivo;
				//print $caminhoArquivo."<hr>";
				if($caminhoArquivo!="." && $caminhoArquivo!=".."){

					if($debug) { print "- Ready > ".$caminhoArquivo." <br>"; }			

					if(is_dir($diretorioPrincipal.$caminhoArquivo)){
						if($debug) { print "- - Ready directory >> ".$caminhoArquivo."<br>"; }	
						readDirectory($diretorioPrincipal.$caminhoArquivo);
					} else {
						$extension = explode(".", $caminhoArquivo);
						if($extension[1] == "json"){
							if($debug) { print "- - Ready JSON file ".$path."/".$caminhoArquivo."... <br>"; }	
							$json = file_get_contents($path."/".$caminhoArquivo);
							if (is_json($json)) {
								if($debug) { print "<strong>- - - Send JSON file for render <br></strong>"; }
								renderJSON($json, $extension[0]);
							}							
						}
					}
				}
			}
		}
		readDirectory($diretorioPrincipal);
	}

	public function getRoomsJson($args){
		/*
		$jsonUnico = object();
		$diretorioPrincipal = $_SERVER['DOCUMENT_ROOT']."/topogigio/files/";

		// Renderiza JSON
		function renderJSON($pathJson){
			$render = new Render();

			// Monta objeto com o JSON e envia pro template
			$jsonDecoded = json_decode($pathJson);
			//print_r($jsonDecoded);
			//print_r("<hr>");
			$render->add('quarto', $jsonDecoded);
			print $render->render('room');
			die;
		}

		// Verifica se é JSON
		function is_json($input) {
		    $input = trim($input);
		    if (substr($input,0,1)!='{' OR substr($input,-1,1)!='}')
		        return false;
		    return is_array(@json_decode($input, true));
		}

		function readDirectory($path){

			$diretorioPrincipal = $_SERVER['DOCUMENT_ROOT']."/topogigio/files/";
			$debug = false;

			$diretorio = dir($path);
			while($arquivo = $diretorio -> read()){
				$caminhoArquivo = $arquivo;
				//print $caminhoArquivo."<hr>";
				if($caminhoArquivo!="." && $caminhoArquivo!=".."){

					if($debug) { print "- Lendo... ".$caminhoArquivo." <br>"; }					

					if(is_dir($diretorioPrincipal.$caminhoArquivo)){
						if($debug) { print "- - Encontrou um diret&oacute;rio... ".$caminhoArquivo.", lendo diretorio <br>"; }	
						readDirectory($diretorioPrincipal.$caminhoArquivo);
					} else {
						$extension = end(explode(".", $caminhoArquivo));
						if($extension == "json"){
							if($debug) { print "- - Encontrou um arquivo JSON... ".$path."/".$caminhoArquivo.", lendo arquivo... <br>"; }	
							$json = file_get_contents($path."/".$caminhoArquivo);
							if (is_json($json)) {
								if($debug) { print "<strong>- - - Enviando JSON para renderizar <br></strong>"; }
								renderJSON($json);
							}							
						}
					}
				}
			}
		}
		readDirectory($diretorioPrincipal);
		*/
	}

}

?>
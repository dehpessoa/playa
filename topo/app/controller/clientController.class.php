<?php

require_once(model_path."Client.class.php");
require_once( core_path.'email.class.php');
require_once( lib_path.'random/random.class.php');

class clientController extends Controller{

	public function index($args){
		$render = new Render();

		$client = new Client();
		$email = $this->pick('email','false');
		$email = ( preg_match("/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i", $email ) )? $email : '' ;
		$client->load( array('email',$email));

		function hideString($string){
			$last = substr($string, -4);
			$string = str_repeat("*", strlen($string) - 4); 
			return $string . $last;
		}

		if(  $client->id_huesped ){
			$dataClient = array(	'first_name' => hideString($client->nombre),
									'last_name' => hideString($client->apellidos),
									'phone' => hideString($client->telefono));			
			$render->json(array('result'=>'success','data'=> $dataClient ));
		}else{
			$render->json(array('result'=>'error','data'=>'User not founded!'));
		}
		
	}

	public function submit($args){
		$render = new Render();

		$json = $this->pick('send');
		$email = ($json['email'])?$json['email']:'';

		if(!preg_match("/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i", $email )){
			$render->json(array('result'=>'error','data'=>'Email inválido'));
			return false;
		}

		$client = new Client();
		$email = ( preg_match("/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i", $email ) )? $email : '' ;
		$client->load( array('email',$email));			

		$client->email     = ($json['email'])? $json['email'] : $client->email;
		$client->nombre    = ($json['first_name'])? trim($json['first_name']) : trim ($client->nombre);
		$client->apellidos = ($json['last_name'])? trim($json['last_name']) : trim($client->apellidos);
		$client->telefono  = ($json['phone'])? $json['phone'] : $client->telefono;

		if(  $client->id_huesped ){
			//$render->json(array('result'=>'success','data'=>'User founded!'));
			$client->save();
		}else{
			$client->create();			
			//$render->json(array('result'=>'error','data'=>'User not founded!'));
		}		

		$main_reserve = false;
		if($json['data']['rooms']){
			$ret = new stdClass();
			$ret->name = $client->nombre . ' ' . $client->apellidos;
			$ret->last_name = $client->apellidos;
			$ret->confirmNumber = 0;
			$ret->price = 0;
			$ret->nights = 0;
			$ret->guests = 0;
			$ret->rooms = array();
			$ret->offers = array();
			$ret->date_arrival = false;
			$ret->date_departure = false;

			foreach ($json['data']['rooms'] as $key => $value) {
				$reserve = new Reservation();
				$reserve->id_casa = $value['id'];
				$reserve->id_huesped = $client->id_huesped;
				$reserve->comentarios = $json['comments'];
				$reserve->promo_code = $json['promotion_code'];
				$reserve->created = date("Y-m-d H:i:s");//$json['promotion_code'];
				$reserve->no_personas = $json['data']['rooms'][$key]['guests'] > 0? $json['data']['rooms'][$key]['guests'] : 1 ;
				$reserve->no_punders = ( isset( $json['data']['rooms'][$key]['children'] ) )? count($json['data']['rooms'][$key]['children']) : 0 ;
				$reserve->no_padults = $reserve->no_personas - $reserve->no_punders;				
				$offers = array();

				$extra_by_adults   = max( $reserve->no_personas - 2 - $reserve->no_punders, 0 );
				$extra_by_children = max( $reserve->no_personas - 2 - $extra_by_adults, 0 );
				//var extra_adults = Math.max( room.guests - 2 - room.children.length, 0 );
				//var extra_children = Math.max( room.guests - 2 - extra_adults, 0 );
				if( $main_reserve ){ $reserve->id_parent = $main_reserve->id_reservacion; };

				if( isset($json['data']['offers'])){
					foreach ($json['data']['offers'] as $keyO => $value) {
						array_push($offers, $json['data']['offers'][$keyO]['id']);
					}
				}

				$reserve->policy = join($offers, ';');

				$rand = new Random();
				$reserve->id_unique = $rand->random_text(6, true, true, true);

				$reserve->create();
				if( !$main_reserve ){ $main_reserve = $reserve; };

				$relation_room_client = new ReservationClientRel();
				$relation_room_client->id_reservacion = $reserve->id_reservacion;
				$relation_room_client->id_huesped = $reserve->id_huesped;
				$relation_room_client->create();
				
				if( !in_array($json['data']['rooms'][$key]['name'], $ret->rooms) ){ 
					array_push($ret->rooms, $json['data']['rooms'][$key]['name']);
				};
				//$ret->nights += count($json['data']['rooms'][$key]['days']);
				//$ret->price += $json['data']['rooms'][$key]['value'];
				$ret->guests = max ( $reserve->no_personas, $ret->guests);

				foreach ($json['data']['rooms'][$key]['days'] as $keyS => $valueS) {
					//print_r(date('Y-m-d', $valueS / 1000));print_r(' ; ');
					$ret->nights += 1;
					$ret->price += $valueS['price'];
					$ret->price += $extra_by_adults;
					$ret->price += $extra_by_children;
					
					if( !$ret->date_arrival || $valueS['date'] < $ret->date_arrival ){ $ret->date_arrival = $valueS['date']; };
					if( !$ret->date_departure || $valueS['date'] > $ret->date_departure ){ $ret->date_departure = $valueS['date']; };
					$reserve->addDate( date('Y-m-d', $valueS['date'] / 1000) );					
				}	


			};

			if( isset($json['data']['offers'])){
				foreach ($json['data']['offers'] as $key => $value) {
					array_push($ret->offers, $json['data']['offers'][$key]['name']);
				}
			}

			$ret->date_departure = ( $ret->date_departure )? $ret->date_departure + ( 24 * 60 * 60 * 1000 ) : $ret->date_departure; // next morning
			$ret->confirmNumber = $main_reserve->id_reservacion;
			$ret->guests = ( $ret->guests > 0 )? $ret->guests : 1;

			$render->json(array('result'=>'success','data'=>$ret));
		}else{
			$render->json(array('result'=>'error','data'=>'Reserve not created.'));
		}

	}

	public function arrival($args){
		$render = new Render();

		switch (count($args['args'])) {
			// GET
			case 1:
				
				$reservation = new Reservation();
				$reservation->load(array('id_unique', $args['args'][0] ) );

				if( $reservation->id_huesped ){
					$client = new Client();
					$client->load( array('id_huesped',$reservation->id_huesped) );	

					$ret = array();
					$ret['name'] = $client->nombre;
					$ret['last_name'] = $client->apellidos;
					$ret['email'] = $client->email;

					unset($reservation->attributes['id_huesped']);
					unset($reservation->attributes['id_reservacion']);
					unset($reservation->attributes['id_parent']);
					unset($reservation->attributes['id_unique']);

					$render->json(array('result'=>'success','data'=> array( 'client'=> $ret, 'reservation' => $reservation->attributes ) ));
				}else{
					$render->json(array('result'=>'error','data'=>'Reserve is not found.'));
				}

				break;
			// SET
			case 2:

				$reservation = new Reservation();
				$reservation->load(array('id_unique',$this->pick('key', $args['args'][0] )) );

				print_r($reservation);
				die('opa!');				

				if( $reservation->id_huesped ){
					$arrival = new Arrival();
					$arrival->update( array(
						'id_reservacion' => $reservation->id_reservacion,
						'id_huesped' => $reservation->id_huesped,
						'date' =>     $this->pick('flight_date'),
						'airport' =>  $this->pick('flight_airport'),
						'airline' =>  $this->pick('flight_airline'),
						'flight' =>   $this->pick('flight_number'),
						'pvr_arrival' => $this->pick('pvr_arrival'),
						'comeby' =>   $this->pick('comeby'),
						'comments' => $this->pick('comments')
					));
					$arrival->save();
					$render->json(array('result'=>'success','data'=>''));
				}


			default:
				# code...
				break;
		}

	}

	public function contactform($args){
		$render = new Render();

		if( $this->pick('email', false) && $this->pick('comments', false) ){
			$render->add('email', $this->pick('email') );
			$name =  "Contact form - " . utf8_decode($this->pick('first_name', $this->pick('email') )) . ' ' . $this->pick('last_name','');
			$render->add('name', $name );
			$render->add('comments', utf8_decode($this->pick('comments')) );

			$msg = $render->render('emailContact',false);

			$return = Email::sendMsg(Config::$email['default']['Username'], $this->pick('email'), utf8_decode($this->pick('first_name')), $name, $msg);
			
			$render->json(array('result'=>'success','data'=>'Contact sended!'));
		}else{
			$render->json(array('result'=>'fail','data'=>'Contact fail!'));
		}
	}


}

?>
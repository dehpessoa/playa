<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF_8" />
	<title>Playa Escondida _ Concept</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="http://projetos.thbastos.com/playa/css/reset.css">
	<link rel="stylesheet" type="text/css" href="http://projetos.thbastos.com/playa/css/style.css">
	<link rel="stylesheet" type="text/css" href="http://projetos.thbastos.com/playa/css/room.css">

	<script type="text/javascript" src="http://projetos.thbastos.com/playa/js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="http://projetos.thbastos.com/playa/js/jquery-menu.js"></script>
	<script type="text/javascript" src="http://projetos.thbastos.com/playa/js/jquery-propresize.js"></script>
	<script type="text/javascript" src="http://projetos.thbastos.com/playa/js/jquery-scrolladdclass.js"></script>
	<script type="text/javascript" src="http://projetos.thbastos.com/playa/js/jquery-softparalax.js"></script>
	<script type="text/javascript" src="http://projetos.thbastos.com/playa/js/masonry.pkgd.min.js"></script>

	<script type="text/javascript" src="http://projetos.thbastos.com/playa/js/preloader.js"></script>	


	<script type="text/javascript">
		$(function(){
			$('.mobile-menu').click(function(){
				$('#navigation').toggleClass('open');
			});

			var classManager = $('.concept li');
			classManager.scrollAddClass();
			$('#navigation').playaMenu();
			$('[data-propresize="true"]').propResize();
			/*$('body').softParalax({
				targets: $('[data-paralax="true"]')				
			});/*neededClass: 'now-viewing'
		});
	</script>

</head>
<body class="room">	
<?php 
//print "Room tpl <hr><pre>"; print_r($quarto); print "</pre>";
?>

	<div class="content mobile-top-margin">
		<div class="preload"></div>
		<div id="navigation">
			<a href="./index.html" class="logo"></a>
			<nav id="menu">
				<ul class="first">
					<li>
						<i class="fi fi_prices"></i>
						<a href="price.html">Prices / Rooms</a>
					</li>
					<li>
						<i class="fi fi_concept"></i>
						<a href="concept.html">the Concept</a>
					</li>
					<li>
						<i class="fi fi_hotel"></i>
						<a href="the_hotel.html">the Hotel</a>
					</li>
					<li>
						<i class="fi fi_experience"></i>
						<a href="experience.html">the Experience</a>
					</li>
					<li>
						<i class="fi fi_map"></i>
						<a href="map.html">the Map</a>
					</li>
				</ul>
				<img src="http://projetos.thbastos.com/playa/image/border_menu.png">
				<ul class="second">
					<li>
						<i class="fi fi_gallery"></i>
						<a href="#">Gallery / Videos</a>
					</li>
					<li>
						<i class="fi fi_info"></i>
						<a href="general_info.html">General Info</a>
					</li>
					<!-- <li>
						<i class="fi fi_offers"></i>
						<a href="offers.html">Offers</a>
					</li> -->
					<li>
						<i class="fi fi_contact"></i>
						<a href="contact.html">Contact us</a>
					</li>
				</ul>
				<ul class="third">
					<li>
						<i class="fi fi_chat"></i>
						<a href="#">Chat with us</a>
					</li>
				</ul>
				<img src="http://projetos.thbastos.com/playa/image/border_menu.png">
			</nav>
			<div class="language">
				<a class="active" href="#">English</a> / 
				<a href="#">Spanish</a>
			</div>				
			<div class="phone">
				<p>+52 329 291 3641</p>
				<p>Sayulita direct</p>
			</div>			
		<!--<div class="social">
				<a href="#" class=""><i class="fi fi_facebook"></i></a>
				<a href="#" class=""><i class="fi fi_twitter"></i></a>
				<a href="#" class=""><i class="fi fi_instagram"></i></a>
				<a href="#" class=""><i class="fi fi_youtube"></i></a>
			</div>-->

			<button class="mobile-menu lines-button arrow arrow-up" type="button" role="button" aria-label="Toggle Navigation">
				  <span class="lines"></span>
			</button>
		</div>

		<div class="navigation_fixed"></div>

		<div class="inside content_block">

			<div class="must_see" data-propresize="true" data-paralax="true" data-width="981" data-heigth="494">
				<div class='play_button big'></div>
				<div class='bg' style="background-image: url('http://projetos.thbastos.com/playa/content/room/slide.png');"></div>
			</div>

			<div class="head_page">
				<div class="info left">
					<h1><?=$quarto->title?> - <?=$quarto->description_title?></h1>				
					<p><?=$quarto->description_text?></p>
					<img src="<?=$quarto->description_image?>">					
					<p class="legend"><?=$quarto->description_legend?></p>
				</div>
				<div class="features left">
					<h2>Room features</h2>					
					<span>Beds:</span>
					<p><?=$quarto->room_features[0]->Beds?></p>
					<span>Occupancy:</span>
					<p><?=$quarto->room_features[0]->Occupancy?></p>
					<span>View:</span>
					<p><?=$quarto->room_features[0]->View?></p>
					<span>Bathroom:</span>
					<p><?=$quarto->room_features[0]->Bathroom?></p>
					<span>Special Features:</span>
					<p><?=$quarto->room_features[0]->{"Special Features"}?></p>
					<span>Other Features:</span>
					<p><?=$quarto->room_features[0]->{"Other Features"}?></p>
				</div>
			</div>

			<div class="special">
				<h1>Special Features</h1>
				<ul>
					<?php foreach($quarto->special_features as $valueSpFeatures): ?>
						<li>
							<img src="<?=$valueSpFeatures->image?>">
							<h2><?=$valueSpFeatures->title?></h2>
							<p><?=$valueSpFeatures->text?></p>
						</li>
						<li class="separate">&nbsp;</li>
					<?php endforeach; ?>				
				</ul>
			</div>

			<div class="other_rooms">
				<h1>
					Not quite what you were looking for?<br />
					Give these other rooms a try.
				</h1>
				<ul>
					<?php foreach($quarto->outhers_rooms as $valueOutherRooms): ?>
						<li>
							<img src="<?=$valueOutherRooms->image?>">
							<h2><?=$valueOutherRooms->title?></h2>
							<p><?=$valueOutherRooms->prices?></p>
						</li>
						<li class="separate">&nbsp;</li>
					<?php endforeach; ?>
				</ul>
			</div>
			
			<div class="footer">
				<div class="site_map">
					<ul>
						<li><h1>Online Store</h1></li>
						<li><a href="#">Surf</a></li>
						<li><a href="#">Golf</li>
						<li><a href="#">Relax & Revitalize</li>
						<li><a href="#">Yoga</a></li>
						<li><a href="#">Adventure</a></li>
						<li><a href="#">Nature</a></li>
						<li><a href="#">Gourmet</a></li>
						<li><a href="#">Romance</a></li>
					</ul>
					<ul>
						<li><h1>Get Help</h1></li>
						<li><a href="#">Contact us</a></li>
						<li><a href="#">Chat</li>
						<li><a href="#">FAQ</li>
						<li><a href="#">Location</a></li>
					</ul>
					<ul>
						<li><h1>Connect</h1></li>
						<li><a href="#">Facebook</a></li>
						<li><a href="#">Twitter</li>
						<li><a href="#">Youtube</li>
						<li><a href="#">Instagram</a></li>
					</ul>
					<ul>
						<li><h1>Language</h1></li>
						<li><a href="#" class="active">english</a> / <a href="#">spanish</a></li>
					</ul>
					<ul>
						<li><h1>Information</h1></li>
						<li>
							[415] 259 4748 <br />
							[888] 445 0103 toll free
						</li>
						<li>
							+1 52 329 291 3641
						</li>
						<li>
							Sayulita, Riviera Nayarit <br />
							Local Time: 07:33:17
							AM
						</li>
						<li>
							reservations@playa-escondida.com
						</li>
					</ul>
				</div>
				<div class="partner">
					<img src="image/partners.png">
				</div>
				<div class="copyright">
					<p>&copy; Playa Escondida. All Rights Reserved.</p>
					<a href="http://www.reinostudio.com.br">
						<img src="http://projetos.thbastos.com/playa/image/logo_reino.png">
					</a>
				</div>
			</div>

		</div>		
	</div>

</body>
</html>
<?php

$router->add('teste','teste',null);
$router->add('index','homeController');

$router->add('rooms','roomsController');
$router->add('rooms/list','roomsController', null, 'roomsList');
$router->add('rooms/render','roomsController', null, 'roomsRender');

$router->add('client','clientController');
$router->add('client/submit','clientController', null, 'submit');
$router->add('client/contact','clientController', null, 'contactform');
$router->add('client/arrival','clientController', null, 'arrival');

$router->add('weather','weatherController', null);

/*$router->add('pages/','homeController');

$router->add('json/teste','jsonController');

$router->add('templates','templatesController',null,'general');
$router->add('templates/sidebar','templatesController',null,'sidebar');
$router->add('templates/alerts','templatesController',null,'alerts');
$router->add('templates/dialogs','templatesController',null,'dialog');

$router->add('user/login','userController',null,'logar');
$router->add('user/logout','userController',null,'deslogar');
$router->add('user/infos','userController',null,'infos');

$router->add('users','userController',null,'page');

$router->add('player','playersController',null,'player');
$router->add('players','playersController');
$router->add('players/status','playersController',null,'status');
/* audios */
/*$router->add('audios','audiosController');

$router->add('clientes','clientesController');

$router->add('playlists','playlistsController');
$router->add('playlist', 'playlistsController',null, 'individual');

$router->add('reports', 'reportsController');*/

$router->add('debbug', 'debbugController');



class Config{
	/* */	
	
	public static $db = array(
		'default' => array(
			'engine'=>'mysql',
			'host'=>'',
			'login'=>'',
			'password'=>'',
			'database'=>'',
			'pre'  =>'',
		)
	);
	
	public static $email = array(
		'default' => array(
			'Host'=>'',
			'SMTPAuth'=>'',
			'SMTPSecure'=>'tsl',
			'Port'=>'',
			'SMTPAuth'=> '',
			'SMTPSecure'=>'',
			'Port'=> '',
			'Username'=>'',
			'Password'=>'',
			'Admin' => '',
			'AddReplyTo'=>array('', ''),
			'SetFrom'=>array('', '')
		)	
	);	
	
	public static $imagens = array(
		'mini'=>array(50,50),
		'thumb'=>array(100,100),
		'grande'=>array(500,500),
		'gigante'=>array(2500,2500)
	);
	
	
};
	
?>
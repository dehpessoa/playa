<?php

class Rooms extends ModelCollection{
	public $table = 'casas';
	public $model = null;

	public function getAll( $startLimit = 0, $endLimit = 10 ){
		//return $this->load( false, "location DESC, summer_price ASC" );
		$sql = "SELECT * 
				FROM casas AS main 
				LEFT JOIN ( 
					SELECT cats.id_location AS category, MIN(cats.summer_price) AS max_price 
					FROM casas AS cats 
					GROUP BY location 
					ORDER BY max_price ASC 
				) AS cats ON cats.category = main.id_location
				LEFT JOIN ( 
					SELECT *
					FROM room_location
				) as o ON o.id_location = main.id_location
				ORDER BY o.order, cats.max_price ASC , cats.category, main.summer_price ASC";

		$matchs = db::sql($sql);
		$ret = array();

		foreach($matchs as $key=>$line){
			$newItem = clone $this->model;
			$newItem->update($line);
			
			if( method_exists( $this, 'processLoad' ) ){
				$newItem = call_user_method('processLoad', $this, $newItem, $line );
			}
			$ret[] = $newItem;

		}

		$this->models = $ret;

		return ($ret);
	}
}

class Room extends ModelCRUD{

	public $defaults = array(
		'id_casa' 		=>	0,
		'casa' 			=> '',
		'color' 		=> '',
		'summer_price' 	=> 0,
		'location' 		=> '',
		'description'	=> '',
		'order'			=> ''
	);

	public $table = 'casas';
	public $status_flag = false;
	public $key_attribute = 'id_casa';

	public function loadUnitDays($from = false, $to = false){
		$unity = $this->id_casa;

		if( !$unity ){ return false; }
		if( !$to ){ $to = $from; }

		$qry = "SELECT * FROM room_nights 
					LEFT JOIN ( SELECT fecha as fecha FROM reservaciones, fechas_reservaciones 
							WHERE reservaciones.id_reservacion = fechas_reservaciones.id_reservacion 
								AND ( fecha BETWEEN :from AND :to ) 
								AND reservaciones.id_casa = :unity ) as fecha ON fecha.fecha = room_nights.date				
				WHERE ( date BETWEEN :from AND :to ) 
					AND id_casa = :unity
					GROUP BY date";

		$matchs = db::sql( $qry ,
			array(
				array( ':unity', $unity ), 
				array( ':from', $from ), 
				array( ':to', $to )
			)
		);
	
		return ( count($matchs) > 0 )?$matchs:array();
	}

	public function loadUnitDaysTypes($from = false, $to = false){
		$unity = $this->id_casa;

		if( !$unity ){ return false; }
		if( !$to ){ $to = $from; }

		$qry = "SELECT r.*, TRUNCATE(r.price,0) as price FROM (
					SELECT * FROM (
						SELECT t.*, 0 as priority FROM room_nights as t								
							WHERE ( t.date BETWEEN :from AND :to )
							GROUP BY t.price
						UNION SELECT l.*, 1 as priority FROM room_nights as l					
							WHERE ( l.date BETWEEN :from AND :to )
							AND l.id_casa = :unity
							GROUP BY l.price
						) as g ORDER BY g.priority DESC					
				) as r GROUP BY r.color_selected ASC";

		/*
		print_r( db::previewSQL( $qry ,
			array( 
				array( ':unity', $unity ),
				array( ':from', $from ), 
				array( ':to', $to )
			)
		) );
		*/

		$matchs = db::sql( $qry ,
			array( 
				array( ':unity', $unity ),
				array( ':from', $from ), 
				array( ':to', $to )
			)
		);
	
		return ( count($matchs) > 0 )?$matchs:array();
	}

	public function getDayPrice( $day = false ){
		$rate = '';

		if( $day ){
			$day_split = explode("-",$day);
			if($day >= "2013-12-20" && $day <= "2014-01-05"){
				$rate = "xmas_price";
			}elseif($day_split[1]=='5' || $day_split[1]=='6' || $day_split[1]=='7' || $day_split[1]=='8' || $day_split[1]=='9' || $day_split[1]=='10'){
				$rate = "summer_price";
			}elseif($day_split[1]=='11' || $day_split[1]=='12' || $day_split[1]=='1' || $day_split[1]=='2' || $day_split[1]=='3' || $day_split[1]=='4'){
				$rate = "winter_price";
			}
		};

		$this->update( 'day_price', (property_exists ( $this, $rate ) )? $this->$rate : max( $this->summer_price, $this->winter_price, $this->xmas_price  ));	

		return $this->day_price;
	}

}

?>
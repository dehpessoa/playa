<?php
class Weather{
	public function getData( $url ){

 		if( array_pop( explode( '.' , $url )  ) == 'xml' ){
 			if(!$load_forecast_current = simplexml_load_file($url)){ return false; }
 		}else{
 			$json = file_get_contents($url);
			$load_forecast_current = json_decode($json);
 		}	

		return $load_forecast_current;

	}
}
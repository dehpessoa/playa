<?php
class Client extends ModelCRUD{

	public $defaults = array(
		'id_huesped' => 0,
		'email' => 0,
		'nombre' => '',
		'apellidos' => '',
		'telefono' => '',
	);

	public $table = 'huespedes';

	public $status_flag = false;
	public $key_attribute = 'id_huesped';

}

class Reservation extends ModelCRUD
{
	public $defaults = array(
		'id_reservacion' => 0,
		'id_huesped' => 0,
		'id_casa' => 0,
		'id_parent' => null,
		'comentarios' => '',
		'promo_code' => '',
		'created' => null,
		'special_request' => '',
		'no_personas' => 1,
		'no_padults' => 1,
		'no_punders' => 0,
		'id_unique' => '',
		'status' => 'pending',
		'policy' => ''
	);

	public $table = 'reservaciones';

	public $status_flag = false;
	public $key_attribute = 'id_reservacion';	

	public function addDate( $date ){
		$sql = "INSERT INTO fechas_reservaciones VALUES (:id_reservacion, :day)";

		$ret = db::sql( $sql, array( 
			array(':id_reservacion', $this->id_reservacion ),
			array(':day', $date )
		) );
		return $ret;
	}
}

class ReservationClientRel extends ModelCRUD
{
	public $defaults = array(
		'id_reservacion' => 0,
		'id_huesped' => 0
	);

	public $table = 'reservaciones_huespedes_rel';

	public $status_flag = false;
	public $key_attribute = 'id';	
}

class Day extends ModelCRUD
{

	public $defaults = array(
		'id_reservacion' => 0,
		'fecha' => 0,
	);

	public $table = 'reservaciones';

	public $status_flag = false;
	public $key_attribute = 'id_reservacion';
	
}

class Arrival extends ModelCRUD
{
	public $defaults = array(
		'id_reservacion' => 0,
		'id_huesped' => 0,
		'date' => '',
		'airport' => '',
		'airline' => '',
		'flight' => '',
		'pvr_arrival' => '',
		'comeby' => '',
		'comments' => ''
	);

	public $table = 'reservaciones_arrival';

	public $status_flag = false;
	public $key_attribute = 'id_reservacion';	
}
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/home_mobile.css">
	<meta name="viewport" content="width=device-width, initial-scel=1">
	<?php include('head.html'); ?>
	<?php include('style.html'); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="js/jquery-menu.js"></script>
	<script type="text/javascript" src="js/jquery-propresize.js"></script>
	<script type="text/javascript" src="js/jquery-scrolladdclass.js"></script>
	<script type="text/javascript" src="js/jquery-softparalax.js"></script>
	<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>

	<script src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script>

	<script type="text/javascript" src="js/weather_widget.js"></script>
	<script type="text/javascript" src="js/lightbox.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/preloader.js"></script>

	<script type="text/javascript">
		$(function(){

			if( weather ) weather.getData();

			var classManager = $('.content_masonry li, .must_see');
			classManager.scrollAddClass();
			$('#navigation').playaMenu();
			$('[data-propresize="true"]').not('li').propResize();
			$('li[data-propresize="true"]').propResize({
				refElem: $('li[data-propresize="true"].square').first()
			});
			/*$('body').softParalax({
				targets: $('[data-paralax="true"]')
			});/*neededClass: 'now-viewing'*/
			var container = $('.content_masonry .concept')[0];
			var msnry = new Masonry( container, {
			  itemSelector: 'li',
			  transitionDuration: 0
			});/**/
			classManager.on('new-viewed',function(){
				msnry.layout();
				setTimeout(function(){msnry.layout();},500);
				setTimeout(function(){msnry.layout();},1000);
			});
			$(window).resize(function(){
				msnry.layout();
				setTimeout(function(){msnry.layout();},500);
				setTimeout(function(){msnry.layout();},1000);
			});
			$(window).on('new-viewed',function(){
				msnry.layout();
				setTimeout(function(){msnry.layout();},500);
				setTimeout(function(){msnry.layout();},1000);
			});
			$(window).resize();

			/*$('.slideshow .bullets span').click(function(){
				var href = $(this).data('href');
				var actual = $('.slideshow li.active');
				var future = $('.slideshow li').eq( href );
				var next = ($('.slideshow li').length > href + 1 )? href + 1 : 0 ;
				if( actual.length ){
					var actualPlayer = $f(actual.find('iframe')[0]);
					actualPlayer.api('pause');
					actualPlayer.addEvent('finish',function(){});
					actual.removeClass('active');
				}

				var futurePlayer = $f(future.find('iframe')[0]);
				futurePlayer.addEvent('ready', function() {
					console.log('ready');
					futurePlayer.api('play');
					futurePlayer.addEvent('finish',function() {
						console.log('finish');
						$('.slideshow .bullets span').eq(next).click();
					});
				});
				if( true ){
					futurePlayer.api('play');
					//$f(future.find('iframe')[0]).play();
					//future.find('iframe').attr('src', future.find('video source').data('src') );
					//future.find('iframe')[0].load();
				}else{
					futurePlayer.api('play');
				}

				future.addClass('active');
			}).eq(0).click();*/
			$('.slideshow .bullets span').click(function(){
				var href = $(this).data('href');
				console.log( href );
				// bullets
				$('.slideshow .bullets span.active').removeClass('active');
				$('.slideshow .bullets span').eq( href ).addClass('active');
				// videos
				var actual = $('.slideshow li.active');
				var future = $('.slideshow li').eq( href );
				var next = ($('.slideshow li').length > href + 1 )? href + 1 : 0 ;
				if( actual.length ){
					actual.find('video')[0].pause();
					actual.find('video')[0].onended = function() {};
					actual.removeClass('active');
				}

				future.find('video')[0].onended = function() {
					console.log('video ended', next);
					$('.slideshow .bullets span').eq(next).click();
				};
				if( future.find('video source').attr('src') == '' ){
					future.find('video source').attr('src', future.find('video source').data('src') );
					future.find('video')[0].load();
				}else{
					future.find('video')[0].play();
				}

				future.addClass('active');
			}).eq(0).click();
		});
	</script>
</head>
<body class="home_mobile">
	<?php include('navigation.html'); ?>
	<?php /*<img class="banner_home" src="image/home_mobile_map-compressor.jpg">*/ ?>
	<div class="must_see" data-propresize="true" data-paralax="true" data-width="981" data-heigth="494" data-active="slider" rel="cover">
		<a class='play_button big'
			href="#"></a>
		<div class='bg' style="background-image: url('http://www.playa-escondida.com/image/home_mobile_map-compressor.jpg');"></div>
		<a href="https://player.vimeo.com/external/135983139.sd.mp4?s=54f80fb87c200917d9f7f6f4b0042f98&profile_id=112" data-cover="http://www.playa-escondida.com/image/home_mobile_map-compressor.jpg" rel="cover"></a>
	</div>
	<div class="menu">
		<ul>
			<li class="border">
				<a href="http://www.playa-escondida.com/prices.html">
					<i class="sprite_hm_prices"></i>
					<p>PRICES</p>
				</a>
			</li>
			<li>
				<a href="http://www.playa-escondida.com/availabilities.html">
					<i class="sprite_hm_book"></i>
					<p>BOOK NOW</p>
				</a>
			</li>
			<li class="border">
				<a href="http://www.playa-escondida.com/concept.html">
					<i class="sprite_hm_concept"></i>
					<P>THE CONCEPT</P>
				</a>
			</li>
			<li>
				<a href="http://www.playa-escondida.com/experience.html">
					<i class="sprite_hm_experience"></i>
					<p>THE EXPERIENCE</p>
				</a>
			</li>
			<li class="border">
				<a href="http://www.playa-escondida.com/the_hotel.html">
					<i class="sprite_hm_hotel"></i>
					<p>THE HOTEL</p>
				</a>
			</li>
			<li>
				<a href="http://www.playa-escondida.com/map.html">
					<i class="sprite_hm_map"></i>
					<p>THE MAP</p>
				</a>
			</li>
		</ul>
	</div>
	<a href="http://playa-escondida.com?desktop-version=1" class="btn_go">GO TO THE DESKTOP VERSION</a>
	<div class="information">
		<h1>LANGUAGE</h1>
		<p>english/ spanish</p>
	
		
		
		<h1>INFORMATION</h1>
		<p>[415] 259 4748<br />
		[888] 445 0103 toll free</p>
		<p>+1 52 [329] 291 3641</p>
		<p>Sayulita, Riviera Nayarit<br />
		Local Time: <?php date_default_timezone_set('America/Los_Angeles'); date('h:iA');?></p>
		<p>reservations@playa-escondida.com</p>
	</div>
	<footer>
		<img src="image/logo_reino.png">
	</footer>
</body>
</html>
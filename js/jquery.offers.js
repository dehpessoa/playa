$(function(){

   	// Change tabs
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active");
		$(this).addClass("active");
	});

	// Change inside tabs
	$(".beach_front ul li").click(function() {
		
		var four = $(".four_nights_content");
		var five = $(".five_nights_content");

		$(".beach_front ul li").removeClass("active");
		$(this).addClass("active");

		var idTab = $(this).attr('id');
		console.log("idTab: " + idTab);

		if(idTab == "beach_three_nights"){
			four.slideUp();
			five.slideUp();
		} else if(idTab == "beach_four_nights"){
			four.slideDown().css('display','block');
			five.slideUp();
		} else if(idTab == "beach_five_nights"){
			four.slideDown().css('display','block');
			five.slideDown().css('display','block');
		}
		
	});

});
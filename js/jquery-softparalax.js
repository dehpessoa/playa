;(function ( $, window, document, undefined ) {

    


    var pluginName = 'softParalax',

        defaults = {

            propertyName: "value"

        };



    // The actual plugin constructor

    function Plugin( element, options ) {

        this.element = element;

        this.options = $.extend( {}, defaults, options) ;

        this.options.targets = this.options.targets || this.element;
        
        this._defaults = defaults;

        this._name = pluginName;

        
        this.init();

    }



    Plugin.prototype.init = function () {


        var self = this;
        var scrollId, startTime, scrollCount = 0;


        $(window).scroll(function(){  

            if( !startTime ){
                console.log( "runnByZeror", scrollCount, scrollId, Date.now() - startTime);   
                startTime = Date.now();
                //
                self.repos( self.options.targets );
            }

            if( scrollId ){
                //console.log( "clearByDelay", scrollCount, scrollId, Date.now() - startTime);
                clearTimeout(scrollId);                
                //
            };
            scrollCount++;

            // for big jumps like mouse wheel
            scrollId = setTimeout(function(){
                console.log( "runnByDelay", scrollCount, scrollId, Date.now() - startTime );                
                //
                scrollCount = 0;
                scrollId = false;
                startTime = 0;

                self.repos( self.options.targets );

            }, 30 );

            // for continues process like mouse middle button press scroll
            if(  Date.now() - startTime > 20){
                console.log( "runnByTimer", scrollCount, scrollId, Date.now() - startTime );            
                clearTimeout(scrollId);
                //
                scrollCount = 0;
                scrollId = false;
                startTime = false;

                //self.repos( self.options.targets );
            }


        }).scroll();
    };


    Plugin.prototype.repos = function( targets ){

        var self = this;

        var topOfWindow = $(window).scrollTop();
        var bottomOfWindow = $(window).scrollTop() + $(window).height();
        var heightOfWindow = $(window).height();

        $( targets ).each(function(){

            if( self.options.neededClass && !$(this).hasClass(self.options.neededClass) ){ return; }
            if( self.options.ignoredClass && $(this).hasClass(self.options.ignoredClass) ){ return; }

            var topThis = $(this).offset().top;
            var bottomThis = $(this).offset().top + $(this).height();

            if( topThis < bottomOfWindow && bottomThis > topOfWindow ){
                //$(this).addClass( self.options.class );
                // calculo baixo
                var propBottom = (( topOfWindow - topThis ) / heightOfWindow );
                var propTop    = (( bottomOfWindow - bottomThis ) / heightOfWindow );
                //$(this).css({ "background-position": '0px ' + propBottom * 20 + "%" });
                $(this).find('.bg').css({ "top": Math.floor( propBottom * 20 * 10000)/10000 + "%" });
            }else{
                //console.log( this, topThis, bottomOfWindow, bottomThis, topOfWindow );
                //$(this).css({ opacity: 1 });
            }

        });
    	
    };


    // A really lightweight plugin wrapper around the constructor, 

    // preventing against multiple instantiations

    $.fn[pluginName] = function ( options ) {

        return this.each(function () {

            if (!$.data(this, 'plugin_' + pluginName)) {

                $.data(this, 'plugin_' + pluginName, 

                new Plugin( this, options ));

            }

        });

    }



})( jQuery, window, document );
var tooltip = {
	_delayer: false,
	$elem: $('<div id="tooltip" />'),
	init: function(){
		$('body').append( this.$elem.addClass('hidden') );
	},
	show: function( anchor, content, css ){
		var self = this;

		if( this.$elem.hasClass( 'hidden' ) ){
			this.$elem
				.html( content )
				.css({ 'left': anchor.offset().left + ( anchor.outerWidth() / 2 ) - ( this.$elem.outerWidth() / 2 ) , 'top': anchor.offset().top - 50})
				.removeClass( 'hidden' );

			if( css ) this.$elem.css( css );
			
			clearTimeout( this._delayer );
			this._delayer = setTimeout( function(){ self.hide(); }, 1500 );
		}else{	
			this.hide();
			clearTimeout( this._delayer );
			this._delayer = setTimeout( function(){ 
				self.show( anchor, content, css );
			}, 200 );
		}
	},
	hide: function(){
		this.$elem.addClass('hidden');
	}
}


var lists = {
	urlList: config.serverUrl + 'rooms/list/all',
	$elList: false,
	$elGrid: false,
	list: {},
	con: false,
	_openedLabels:{},
	filter:{
		rooms: [],
		price_low: 0,
		price_high: 1000
	},
	init: function(){
		var self = this;

		/* FIXED LABELS */
		/*
		var delayScroll;		
		$(window).scroll(function(){
			if( delayScroll ) return;
			delayScroll = setTimeout( function(){
				$('.label.open').each(function(){
					if( $(this).scrollTop() < $(window).scrollTop() ){
						$(this).addClass('fixed');
					}else{  						
						$(this).removeClass('fixed');
					}
				});
				delayScroll = false;
			}, 5 );
		});*/

		$(".form-group .btnmobilefeatures").click(function(){
			$(".form-group .btnfilters").parent().addClass('hide');
			$(".form-group button.btnmorefilters").click();

			if($("#filters").hasClass('hide')){
				$(this).html(t.g('SHOW FILTERS'));
			} else {
				$(this).html(t.g('HIDE FILTERS'));
			}
			//$(".form-group button.btnmorefilters").html( $(".form-group .btnfilters").html() == t.g('SHOW FEATURES') ? t.g('HIDE FEATURES') : t.g('HIDE FEATURES') );
		});

		$(".form-group .btnbooking").click(function(){
			$('#avaliable').toggleClass('hide-mobile');

			if($("#avaliable").hasClass('hide-mobile')){
				$(this).html(t.g('SHOW AVAILABILITY'));
			} else {
				$(this).html(t.g('HIDE AVAILABILITY'));
			}
		});


		$(".form-group button.btnmoreavailability").click(function(){
			if($("#avaliable").hasClass('hide-mobile')){
				$(this).html(t.g('HIDE AVAILABILITY'));
			} else {
				$(this).html(t.g('SHOW AVAILABILITY'));
			}/**/
			$("#avaliable").toggleClass('hide-mobile');
			$(window).resize();
			return false;
		});

		$(".form-group button.btnmorefilters").click(function(){
			if($("#filters").hasClass('hide')){
				$(this).html(t.g('HIDE FILTERS'));
			} else {
				$(this).html(t.g('SHOW FILTERS'));
			}/**/
			$("#filters").toggleClass('hide');
			$(window).resize();
			return false;
		});

		$( "#date-init, #date-finish" ).datepicker({
			inline: true,
			"dateFormat": "dd/mm/yy",
			minDate: ( new Date() ),
			beforeShowDay : function( date ){
				return [ ( date > ( new Date() ) ), ( (date||"").toString() == ($('#date-init').datepicker('getDate')||"").toString() )? 'start-date' : '' , null ];
			},
		    onClose: function(dateText, inst) 
		    { 
		        $(this).attr("disabled", false);
		    },
		    beforeShow: function(input, inst) 
		    {
		        $(this).attr("disabled", true);
		    }		
		});

		$('.select-style select').on('focus',function(){ $(this).parent().addClass('focus'); });
		$('.select-style select').on('blur',function(){ $(this).parent().removeClass('focus'); });

		$('#date-init').change(function(e){
			console.log(e);
			$(this).parent().addClass('complete').removeClass('next');

			var first_line_complete = ( $('[name=beds]').val() !== "" && $('[name=guests]').val() !== "" && $('[name=date-init]').val() !== "" );
			var may_have_children   = first_line_complete && ( $('[name=beds]').val() > 1 || $('[name=guests]').val() > 2 );	
			$('#children_choice').toggleClass( 'hide', ( !may_have_children ) );

			if( $('#date-init').val() && $('[name=beds]').val() && $('[name=guests]').val() ){
				self.updateList( function(){
					self.printList('hidden');
					self.mountCalendar( $('#date-init').val(), $('#date-finish').val() );
					self.filterList();				
					self.fadeinList();
					//self.blinkList();
				},$('#date-init').val(), $('#date-init').val());	
			}			
		});

		$('[name=guests]').selectmenu({change: function(){
				$(this).parent().addClass('complete').removeClass('next');
				$('[name=beds]').removeAttr('disabled').parent().removeClass('disabled').addClass('next');
				$('[name=guests]').parent().parent().next().toggleClass('hide', ( $('[name=guests]').val() !== "more" && $('[name=beds]').val() < 2  ));
				$('#more_beds').toggleClass( 'hide', !( $('[name="guests"]').val() == "more" || parseInt( $('[name="beds"]').val() ) > 1 ) );

				var first_line_complete = ( $('[name=beds]').val() !== "" && $('[name=guests]').val() !== "" && $('[name=date-init]').val() !== "" );
				var may_have_children   = first_line_complete && ( $('[name=beds]').val() > 1 || $('[name=guests]').val() > 2 );	
				$('#children_choice').toggleClass( 'hide', ( !may_have_children ) );

				if( $('#date-init').val() && $('[name=beds]').val() && $('[name=guests]').val() ){
					self.updateList( function(){
						self.printList('hidden');
						self.mountCalendar( $('#date-init').val(), $('#date-finish').val() );
						self.filterList();
						self.fadeinList();
					},$('#date-init').val(), $('#date-init').val());	
				}	
			},
			width: '100%'
		});

		$('[name=beds]').selectmenu({change: function(){
				$('[name=beds]').parent().addClass('complete').removeClass('next');
				if( $('[name=beds]').val() > 1 ){ $('[name=beds]').parent().parent().next().removeClass('hide'); }
				$('[name=guests]').parent().parent().next().toggleClass('hide', ( $('[name=guests]').val() < 3 && $('[name=beds]').val() < 2  ));	

				var first_line_complete = ( $('[name=beds]').val() !== "" && $('[name=guests]').val() !== "" && $('[name=date-init]').val() !== "" );
				var may_have_children   = first_line_complete && ( $('[name=beds]').val() > 1 || $('[name=guests]').val() > 2 );		
				$('#children_choice').toggleClass( 'hide', ( !may_have_children ) );

				$('#more_beds').toggleClass( 'hide', !( $('[name="guests"]').val() == "more" || parseInt( $('[name="beds"]').val() ) > 1 ) );

				self.filterList();

				$('#date-init').removeAttr('disabled').parent().removeClass('disabled').addClass('next');
			},
			position: { collision:"flip" },
			width: '100%'
		});

		$('[name="guests"], [name="beds"]').change(function(){
			$('#more_beds').toggleClass( 'hide', !( $('[name="guests"]').val() == "more" || parseInt( $('[name="beds"]').val() ) > 1 ) );
		});

		$("#check-children").change(function(){
			$(this).parent().toggleClass( 'hide', ( $(this).prop('checked') ) ).next().toggleClass( 'hide', ( !$(this).prop('checked') ) );
		});

		$("[name=children_count]").selectmenu({change:function(){
				$('#children_alert').toggleClass( 'hide', !( $(this).val() > 0 ) );
				$('#children_choice').toggleClass( 'hide', !( $(this).val() > 0 ) );

				$('[name=child_age_1]').parent().toggleClass( 'hide', ( !$(this).val()       ) );
				if( !$(this).val()       ) $('[name=child_age_1]').val('');
				$('[name=child_age_2]').parent().toggleClass( 'hide', ( $(this).val() < 2 ) );
				if( $(this).val() == "1" ) $('[name=child_age_2]').val('');
			},
			position: { collision:"flip" },
			width: '100%'
		});

		$("[name=child_age_1],[name=child_age_2]").selectmenu({change:function(){
				var complete = true;
				complete = complete && ( $("[name=child_age_1][disabled]").length == 0 || $("[name=child_age_1]").val() !== "" );
				complete = complete && ( $("[name=child_age_2][disabled]").length == 0 || $("[name=child_age_1]").val() !== "" );
				$('#children_choice, #children_alert').toggleClass( 'hide', ( complete ) );
			},
			width: '100%'
		});

		$("#check-children-alert").change(function(){
			if( !$(this).prop('checked') ){ 
				$('[name=child_age_1]').attr('disabled','disabled').parent().addClass('disabled'); 
				$('[name=child_age_2]').attr('disabled','disabled').parent().addClass('disabled');
			}else{ 
				$('[name=child_age_1]').removeAttr('disabled','disabled').parent().removeClass('disabled');
				$('[name=child_age_2]').removeAttr('disabled','disabled').parent().removeClass('disabled');
			}
		});

		/* ================ LIST */
		$('[name="check-dates"], .check-custom, .filter [name=area], .filter [name=beds], #more_beds input[type="checkbox"]').change(function(){
			self.filterList();
		});

		/* GET INITIAL FILTERS
		*
		*	price.html#/ [ filter-name ] / value
		*
		*	arrival, departure, rooms
		*
		*/
		if( window.location.hash && window.location.hash.length > 2 ){
			var pairs = window.location.hash.replace('#','').split('/');
			if( pairs.length > 1 ){
				for (var i = 0; i < pairs.length; i+=2 ) {
					self.filter[pairs[i]] = pairs[i+1].indexOf(',') > 0 ? pairs[i+1].split(',') : pairs[i+1] ;
				};
			};
		}

		if( this.filter.arrival ){ 
			var tomorrow = new Date( this.filter.arrival.replace('-','/') );
			tomorrow.setDate( tomorrow.getDate() );
			$('#date-init').datepicker("setDate", tomorrow );		
		}

		if( this.filter.departure ){ 
			var tomorrow = new Date( this.filter.departure.replace('-','/') );
			tomorrow.setDate( tomorrow.getDate() );
			$('#date-finish').datepicker("setDate", tomorrow );	
		}/**/

		if( this.filter.arrival || this.filter.departure ){
			self.updateList( function(){
				self.printList('hidden');
				self.mountCalendar( $('#date-init').val(), $('#date-finish').val() );
				self.filterList();
				self.fadeinList();
			},$('#date-init').val(), $('#date-finish').val()); 
		}else{
			this.updateList(function(){
				self.printList('hidden');
				self.filterList();	
				self.fadeinList();			
			});	
		}/**/			
	},
	addRoom: function( room ){
		( room || ( room = {
			price_low: ( Math.floor( 25 * Math.random() ) * 10 ),
			price_high: ( Math.floor( 50 * Math.random() ) * 20 ),
			name: ( "Room name " + ( Math.floor( Math.random() * 1000 ) ) ),
			area: "Playa",
			description: "Canopy King bed, Private Deck, Lounge chairs, hammock, panorama ocean view, wifi, privacy and comfort.",
			img: "http://projetos.thbastos.com/playa/content/price/thumb_01.png",
			url: "www.google.com",
			beds: ( 1 + Math.floor( 3 * Math.random() ) ),
			area: (['beach front','ocean view','garden view'])[( Math.floor( Math.random() * 3 ) )],
			features:( function(){
					var ret = [];
					if( Math.random() > 0.5 ) ret.push('wifi');
					if( Math.random() > 0.5 ) ret.push('tv');
					if( Math.random() > 0.5 ) ret.push('hottube');
					if( Math.random() > 0.5 ) ret.push('sp4');
					return ret;
				})()
		} ) );

		room.$elList = false;
		room.$elGrid = false;

		room.areaId = room.area.toLowerCase().replace( /[^a-z0-9]/gi, '' );

		if( !this.list[ room.areaId ] ) this.list[ room.areaId ] = [];
		this.list[ room.areaId ].unshift( room );
	},
	resetList: function(){
		for (var i = this.list.length - 1; i >= 0; i--) {
			if( this.list[i].$elList ) this.list[i].$elList.remove();  
			if( this.list[i].$elGrid ) this.list[i].$elGrid.remove();  
		}
		this.list = {};
	},
	blinkList: function( force ){
		var self = this;

		self.fadeinList('hover');
		setTimeout( function(){ self.fadeoutList('hover'); }, 700 );

	},
	fadeoutList: function( effect ){
		// prices TABLE
		$('ul.body li').each(function(i,e){
			var elem = $(this);
			setTimeout( function(){ elem.toggleClass( effect || 'hidden', true); }, 15 * i + 50 );
		});

		// prices GRID
		$('ul.rooms li').each(function(i,e){
			var elem = $(this);
			setTimeout( function(){ elem.toggleClass( effect || 'hidden', true); }, 50 * i + 50 );
		});
	},
	fadeinList: function( effect ){
		// prices TABLE
		$('ul.body li').each(function(i,e){
			var elem = $(this);
			setTimeout( function(){ elem.toggleClass( effect || 'hidden', false); }, 15 * i + 50 );
		});

		// prices GRID
		$('ul.rooms li').each(function(i,e){
			var elem = $(this);
			setTimeout( function(){ elem.toggleClass( effect || 'hidden', false); }, 50 * i + 50 );
		});
	},
	updateList: function( callback, startInput, endInput ){
		var self = this;

		var start, end;
		start = end = '';
		if( startInput ){
			var startDate = self.stringToDate(startInput);
			var offsetDate = startDate.getDay();
			startDate.setDate( startDate.getDate() - offsetDate );
			start = "/" + startDate.getFullYear() + "-" + (startDate.getMonth()+1) + "-" + startDate.getDate();
		}

		if( endInput || (endInput = startInput) ){
			var endDate = self.stringToDate(endInput);
			endDate.setDate( endDate.getDate() + 21 - ( offsetDate ? offsetDate + 1 : 0 ) );
			end = "/" + endDate.getFullYear() + "-" + (endDate.getMonth()+1) + "-" + endDate.getDate();
		}

		if( self.con ){ self.con.abort(); self.con = false; }
		self.con = $.ajax({
			url: this.urlList + start + end,
			dataType: "json",
			cache: false,
			beforeSend: function(){
				if( startInput ) $('ul.body').addClass('calendar');
				$('ul.body, ul.rooms').addClass( 'loading' );

				self.fadeoutList();

				var calendars = $('span.calendar');
				calendars.find('div').addClass('hide');
				calendars.find('table').addClass('hide');
				calendars.find('div.loading').removeClass('hide');
			},
			success: function( res ){
				$('ul.body, ul.rooms').removeClass( 'loading' );
				self.con = false;
				//res = JSON.parse( data );
				self.resetList();

				if( res.result ){
					res.data = res.data.reverse();
					for (var i = res.data.length-1; i >= 0; i--) {
						if( res.data[i] == undefined ){
							console.log( i, res.data[i] );
							continue;
						}
						var room = {
							id: res.data[i].id_casa,
							name: res.data[i].casa,
							area: res.data[i].location,
							description: res.data[i].description,
							price_low: parseFloat( res.data[i].summer_price ),
							price_high: parseFloat( res.data[i].winter_price ),
							price_holiday: parseFloat( res.data[i].holidays_price ),
							reserved: res.data[i].dates,
							days: res.data[i].days || [],
							img: 'rooms/images/'  + res.data[i].slug + '/thumb/'  + res.data[i].slug + '_02.jpg',
							url: res.data[i].slug + '.html',
							video: res.data[i].video,
							inconvenience: res.data[i].inconvenience,
							beds: res.data[i].no_bed,
							bednote: res.data[i].bednote,
							room_features: (res.data[i].room_features||'').toLowerCase().split(';'),
							types: (res.data[i].types||[])
						};
						self.addRoom( room );
					};
				}
				if( callback ){
					callback();
				}else{
					self.printList();
					self.filterList();
				}

				
			}
		});
		
		$('.options strong').html( this.list.length );
	},
	printList: function( defaultClass ){
		( defaultClass || ( defaultClass = '' ) );
		var self = this;
		var count = 0;

		$('.tab-price ul.body li').not('.loader').remove();
		$('.tab-accommodation ul li').not('.loader').remove();

		for (prop in this.list) {
		    if (this.list.hasOwnProperty(prop)) {

		    	var $listLabel = $(''+
						'<li class="label">'+
							'<span><a href="#"><div><strong></strong></div></a><i></i></span>' +
							'<span class="prices">' +
								'<span><div style="color: #00979A;"><small>'  + t.g('Lowest') + '</small><strong></strong><small>usd</small></div><div>' + t.g('Summer') + '</div></span>' +						
								'<span><div style="color: #DA7F22;"><small>' + t.g('Highest') + '</small><strong></strong><small>usd</small></div><div>' + t.g('Winter') + '</div></span>' +	
							'</span>'+
							'<span class="features">' +
								'<span class="feature jacuzzi">'  + t.g('jacuzzi') + '</span>'+
								'<span class="feature ac">'  + t.g('ac') + '</span>'+
								'<span class="feature gorf-car">'  + t.g('golf car') + '</span>'+
								'<span class="feature sleeps">'  + t.g('sleeps') + '</span>'+
							'</span>'+
							'<span class="description"><span></span></span>'+
							'<span class="calendar">'+
							'<div>'+
								t.g('nights staying - please mark them in the calendar') +
							'</div>'+							
						'</li>'
					);
		    	$listLabel.attr({id:prop});
		    	$listLabel.addClass(defaultClass);

		    	$listLabel.find('.description span').css({'backgroundImage':'url(./image/price/'+ prop +'.png)'});

		    	$listLabel.click(function(e){
		    		/* close any room content opened */
		    		$('.room_content .close').click();

		    		$(this).toggleClass('open');
		    		if( $(this).hasClass('open') ){
		    			self._openedLabels[ $(this).attr('id') ] = true;
		    			$(this).nextUntil('.label').each(function(i, $el){
		    				$($el).delay(150*i).fadeIn(500);
		    			});
		    		}else{
		    			self._openedLabels[ $(this).attr('id') ] = false;
		    			$($(this).nextUntil('.label').get().reverse()).each(function(i, $el){
		    				$($el).delay(75*i).fadeOut(75);
		    			});
		    		}
		    		$(this).blur();
		    		e.stopPropagation(); // This is the preferred method.
					return false;   
		    	});

		    	if( self._openedLabels[ $listLabel.attr('id') ] ){ 
		    		$listLabel.addClass('open');
		    	};

		    	var min_price = this.list[prop][0].price_low;
		    	var max_price = this.list[prop][0].price_high;

		    	var area = this.list[prop][0].area .split("-");
		    	$listLabel.find('span:eq(0) strong').html( area[0] );
		    	if( area.length > 1 ){
		    		$listLabel.find('span:eq(0) strong').after( $("<div/>").html( area[1] ) );	
		    	}

		    	$listLabel.find("span:eq(0) i").html( this.list[prop].length + " " + (this.list[prop].length>1? t.g('rooms') : t.g('room')) );
		    	$('.tab-price ul:eq(0)').append( $listLabel );

		    	count += this.list[prop].length;

				for (var i = this.list[prop].length - 1; i >= 0; i--) {

					min_price = Math.min( this.list[prop][i].price_low, this.list[prop][i].price_high, min_price );
					max_price = Math.max( this.list[prop][i].price_low, this.list[prop][i].price_high, max_price );

					this.list[prop][i].$elList = $(''+
							'<li class="room">'+
								'<span class="prices">'+
								'</span>'+
								'<span class="calendar">'+
								'	<div class="warn">' + t.g('select_your_check__br_in_date') + '</div>'+
								'	<div class="loading hide">' + t.g('Loading...') + '</div>'+
								'</span>'+
								'<span class="options disabled">'+
								'	<button class="disabled">' + t.g('select the days') + '</button>'+
								'</span>'+
							'</li>'
						).addClass(defaultClass).toggle( ( self._openedLabels[ prop ] || self.filter.arrival ) );

					this.list[prop][i].$elGrid = $(''+
							'<li> ' +
							'	<div class="thumbs"> ' +
							'		<div class="picture"> ' +
							'			<img src="http://projetos.thbastos.com/playa/content/price/thumb_01.png" class="bg"> ' +
							'		</div> ' +
							'		<i class="ic_price_room ic_search"></i> ' +
							'		<a ' +
							'			data-icon="#"'+
							'			data-active="lightbox" '+
							'			style="background:url("http://projetos.thbastos.com/playa/content/home/hotel_areas_01.png");" >'+
							'				<i class="play_button big"></i> ' +					
							'		</a> ' +					
							'		<div class="prices"></div> ' +					
							'	</div> ' +
							'	<div class="info"> ' +
							'		<a href="#"> ' +
							'			<h1>Ixchel Penthouse</h1> ' +
							'			<p>Canopy King bed, Private Deck, Lounge chairs, hammock, panorama ocean view, wifi, privacy and comfort</p> ' +
							'		</a> ' +
							'		<span class="calendar hide">	' +	
							'			<span><strong>Nights staying</strong></span>' + 												
							'			<div class="alert"> ' +
							'				<hr> ' +
							'				<p class="warn">' + t.g('select_your_check_in_date') + '</p> ' +
							'				<hr> ' +
							'			</div> ' +
							'			<div class="loading hide">'+
							'				<hr> ' +
							'				<p class="warn">' + t.g('Loading...') + '</p> ' +
							'				<hr> ' +
							'			</div>'+
							'		</span> ' +
							'		<button class="disabled hide">' + t.g('select some days') + '</button> ' +
							'	</div> ' +
							'</li> '
						).addClass(defaultClass);

					$name = $('<span><a href="#"><strong></strong></a></span>');
					$price_low =  $('<div style="color: #00979A;"><strong></strong><small>usd</small></div>');
					$price_high = $('<div style="color: #DA7F22;"><strong></strong><small>usd</small></div>');

					$table = $('<table class="calendar calendar_price hide"></table>');
					$thead = $('<tr/>');

					for (var k = (['Su','Mo','Tu','We','Th','Fr','Sa']).length - 1; k >= 0; k--) {
						$thead.prepend( $('<th/>').html( (['Su','Mo','Tu','We','Th','Fr','Sa'])[k] ) );
					};
					
					for (var k = 2; k >= 0; k--) {
						var $line = $('<tr/>');
						for (var j = 6; j >= 0; j--) {
							var $cell = $('<td></td>');
							$cell.html( k * 7 + j );
							$cell.click(function(){
								if( !$(this).hasClass("off") ){ 
									$(this).toggleClass("selected"); 
									$(this).css( 'background', '#' + ( $(this).hasClass("selected")? $(this).data('data').select_color : $(this).data('data').color ) ); 
								}
							});	

							$line.prepend( $cell );
						};
						$table.prepend( $line );
					};
					$table.prepend( $thead );


					this.list[prop][i].$elList.find('.warn').click(function(){
						$('html, body').animate({
			                scrollTop: $('#date-init').position().top - ($(window).width() < 720 ? $("#navigation").outerHeight() : 0 ) - 25
			            }, 500, function(){ $('#date-init').focus(); });
					});


					/*
					*      = TABLE DATE EVENTS
					*/

					this.list[prop][i].$elList.on( 'mouseenter', 'td', function( event ){
						if( $(event.target).hasClass('off') ){ tooltip.hide(); return; } 
						if( $(event.target).hasClass('selected') ){ tooltip.hide(); return; }
						tooltip.show( $(event.target), '$ ' + $(this).data('data').price, { 'color': '#' + $(this).data('data').select_color } );
					});

					this.list[prop][i].$elList.click( 'td', function( event ){ 		
						if( $(event.target).hasClass('off') ) return;  
						if( event.target.tagName !== 'TD' ){ return false; }	

						$(this).find('button').toggleClass('disabled', $(this).find('td.selected').length == 0 );
						$(this).find('button').parent().toggleClass('disabled', $(this).find('td.selected').length == 0 );
						$(this).find('button').html( $(this).find('td.selected').length == 0 ? t.g('select the days') : t.g('check out') );
						/*} );
						this.list[i].$elList.find('.options button').click(function(){ */
						var dates = [];
						var room = $(this).data('room');
						var c = $( event.target ).index();
						var l = $( event.target ).parent().index();	
						room.$elGrid.find('.calendar table tr:eq(' + l + ') td:eq(' + c + ')' ).attr( 'class', $( event.target ).attr("class")  );

						room.$elList.find('.selected').each(function(){
							dates.push( $(this).data('data') );
						});

						var children = [];
						if( $("[name=check-children-alert]").prop('checked') && $('[name="child_age_1"]').val() ) children.push( $('[name="child_age_1"]').val() );
						if( $("[name=check-children-alert]").prop('checked') && $('[name="child_age_2"]').val() ) children.push( $('[name="child_age_2"]').val() );

						cart.addRoom({
							id: 		room.id, 
							img:    	room.img,
							name:   	room.name,
							nights: 	dates.length,
							days: 		dates,
							guests: 	$('.filter [name=guests]').val(),
							beds:   	$('.filter [name=beds]').val(),
							children: 	children
						});
					});
					this.list[prop][i].$elList.find('.options button').click(function(){
						if( !$(this).hasClass('disabled') ) cart.show();
					});

					this.list[prop][i].$elGrid.on( 'mouseenter', 'td', function( event ){
						if( $(event.target).hasClass('off') ){ tooltip.hide(); return; } 
						if( $(event.target).hasClass('selected') ){ tooltip.hide(); return; }
						tooltip.show( $(event.target), '$ ' + $(this).data('data').price, { 'color': '#' + $(this).data('data').select_color } );
					});

					this.list[prop][i].$elGrid.click( 'td', function( event ){ 
						if( $(event.target).hasClass('off') ) return;  
						$(this).find('button').toggleClass('disabled', $(this).find('td.selected').length == 0 );
						$(this).find('button').parent().toggleClass('disabled', $(this).find('td.selected').length == 0 );
						$(this).find('button').html( $(this).find('td.selected').length == 0 ? t.g('select your dates') : t.g('check out') );

						var dates = [];
						var room = $(this).data('room');
						var c = $( event.target ).index();
						var l = $( event.target ).parent().index();	
						room.$elList.find('.calendar table tr:eq(' + l + ') td:eq(' + c + ')' ).attr( 'class', $( event.target ).attr("class")  );

						var price = 0;
						room.$elGrid.find('.selected').each(function(){
							price += $(this).data('price') || 0;
							dates.push( $(this).data('data') );
						});

						var children = [];
						if( $("[name=check-children-alert]").prop('checked') && $('[name="child_age_1"]').val() ) children.push( $('[name="child_age_1"]').val() );
						if( $("[name=check-children-alert]").prop('checked') && $('[name="child_age_2"]').val() ) children.push( $('[name="child_age_2"]').val() );

						cart.addRoom({
							id: 	room.id, 
							img:    room.img,
							name:   room.name,
							nights: dates.length,
							value:  price,
							guests: $('.filter [name=guests]').val(),
							beds:   $('.filter [name=beds]').val(),
							children: children,
							days: dates
						});				
					});
					this.list[prop][i].$elGrid.find('.info button').click(function(){
						if( !$(this).hasClass('disabled') )cart.show();
					});

					this.list[prop][i].$elGrid.find('.warn').click(function(e){
						$('html, body').animate({
			                scrollTop: $('#date-init').position().top - ($(window).width() < 720 ? $("#navigation").outerHeight():0) - 25
			            }, 500, function(){ $('#date-init').focus(); });
					});

					/*  ==========  */

					$name.find('strong').html( this.list[prop][i].name );
					this.list[prop][i].$elGrid.find('h1').html( this.list[prop][i].name );

					this.list[prop][i].$elGrid.find('p:eq(0)').html( this.list[prop][i].description );

					$name.find('a').attr('href', this.list[prop][i].url ).click( function(){
						self.mountRoom( 'list', $(this).parent().parent() );
						return false;
					});

					$price_low.find('strong').html('$ '  + this.list[prop][i].price_low );
					$price_high.find('strong').html('$ ' + this.list[prop][i].price_high );

					this.list[prop][i].$elList.find('.options a:eq(1)').attr('href', this.list[prop][i].url );

					this.list[prop][i].$elGrid.find('.thumbs img').attr('src', this.list[prop][i].img );

					if( this.list[prop][i].video ){
						this.list[prop][i].$elList.find('.options a:eq(0)').attr('href', this.list[prop][i].video );
						this.list[prop][i].$elList.find('.options a:eq(0)').attr('data-icon', this.list[prop][i].img );

						this.list[prop][i].$elGrid.find('.thumbs a').attr('href', this.list[prop][i].video );
						this.list[prop][i].$elGrid.find('.thumbs a').attr('data-icon', this.list[prop][i].img );				
					}else{
						this.list[prop][i].$elList.find('.options span').addClass('disabled');
						this.list[prop][i].$elList.find('.options a:eq(0)').removeAttr('data-active');

						this.list[prop][i].$elGrid.find('.thumbs a').addClass('hide');
					}

					this.list[prop][i].$elGrid.find('.info a').attr('href', this.list[prop][i].url ).click( function(){
						self.mountRoom( 'grid', $(this).parent().parent() );
						return false;
					});

					this.list[prop][i].$elGrid.find('.thumbs').click( function(){
						self.mountRoom( 'grid', $(this).parent() );
						return false;
					});


					if( this.list[prop][i].types && this.list[prop][i].types.length ){
						this.list[prop][i].$elGrid.find('.prices').empty();
			    		this.list[prop][i].$elGrid.find('.prices').attr({ class:'prices values-' + this.list[prop][i].types.length });
			    		for (var k = this.list[prop][i].types.length - 1; k >= 0; k--) {
			    			var cell = $('<span></span>');
			    			cell
			    				.css({ background: '#' + this.list[prop][i].types[k].color_unselected });

			    			cell
			    				.html( '$' + this.list[prop][i].types[k].price );

			    			cell
			    				.append( $("<em/>").html( this.list[prop][i].types[k].caption ) );

			    			this.list[prop][i].$elGrid.find('.prices').prepend( cell );
			    		};
					}else{
						this.list[prop][i].$elGrid.find('.prices').append( $('<span><wbr/><em>'+ t.g('Summer')   +'</em></span>').prepend( '$ ' + this.list[prop][i].price_low ) );
						this.list[prop][i].$elGrid.find('.prices').append( $('<span><wbr/><em>'+ t.g('Winter') +'</em></span>').prepend( '$ ' + this.list[prop][i].price_high ) );
			    	}


					this.list[prop][i].$elList.find('.calendar').prepend( $table );
					this.list[prop][i].$elGrid.find('.calendar > span').after( $table.clone( true, true ) );

					this.list[prop][i].$elList.data('room', this.list[prop][i]);
					this.list[prop][i].$elGrid.data('room', this.list[prop][i]);
	
					// Write the prices at table fields
					if( this.list[prop][i].types && this.list[prop][i].types.length ){
						this.list[prop][i].$elList.find('.prices').empty();
			    		this.list[prop][i].$elList.find('.prices').attr({ class:'prices values-' + this.list[prop][i].types.length });
			    		for (var k = this.list[prop][i].types.length - 1; k >= 0; k--) {
			    			var cell = $('<span><div><strong></strong><small>usd</small></div></span>');
			    			cell
			    				.find('div')
			    				.first().css({color: '#' + this.list[prop][i].types[k].color_unselected });

			    			cell
			    				.find('strong').html( '$' + this.list[prop][i].types[k].price );

			    			this.list[prop][i].$elList.find('.prices').prepend( cell );
			    		};
					}else{
	  					this.list[prop][i].$elList.find('.prices').prepend( $('<span/>').append( $price_high ) );
						this.list[prop][i].$elList.find('.prices').prepend( $('<span/>').append( $price_low  ) );  		
			    	}


					this.list[prop][i].$elList.prepend( $name );
					this.list[prop][i].$elList.append( $('<span class="feature jacuzzi"/>').append( $("<div/>").html( this.list[prop][i].room_features.indexOf('jacuzzi') > -1 ? '<i class="true"/>' : '<i class="false"/>' ) ) );
					this.list[prop][i].$elList.append( $('<span class="feature ac"/>').append( $("<div/>").html( this.list[prop][i].room_features.indexOf('ac') > -1 ? '<i class="true"/>' : '<i class="false"/>' ) ) );
					this.list[prop][i].$elList.append( $('<span class="feature golf-car"/>').append( $("<div/>").html( this.list[prop][i].room_features.indexOf('golf-car') > -1 ? '<i class="true"/>' : '<strong>+USD $50</strong><br>(optional)' ) ) );
					this.list[prop][i].$elList.append( $('<span class="feature sleeps"/>').append( $("<div/>").html( (this.list[prop][i].bednote||"").replace(';',' or <br>') ) ) );

					$('.tab-price ul.body').append( this.list[prop][i].$elList.toggle( self._openedLabels[ prop ] !== undefined && self._openedLabels[ prop ] === true ) );
					$('.tab-accommodation ul').append( this.list[prop][i].$elGrid );

					$('.options strong').html( this.list.length );

					if(lightbox) lightbox.scan( this.list[prop][i].$elGrid );
					if(lightbox) lightbox.scan( this.list[prop][i].$elList );
				};	

				// Write prices at label fields
				if( this.list[prop][0].types && this.list[prop][0].types.length ){
		    		$listLabel.find('.prices').empty();
		    		$listLabel.find('.prices').attr({ class:'prices values-' + this.list[prop][0].types.length });
		    		for (var i = this.list[prop][0].types.length - 1; i >= 0; i--) {
		    			var cell = $('<span><div><small></small><strong></strong><small>usd</small></div><div></div></span>');
		    			cell
		    				.find('div')
		    				.first().css({color: '#' + this.list[prop][0].types[i].color_unselected })
		    				.find('small').first().html( this.list[prop][0].types[i].caption );

		    			cell
		    				.find('div').last()
		    				.html( this.list[prop][0].types[i].caption );

		    			cell
		    				.find('strong').html( '$' + this.list[prop][0].types[i].price );

		    			$listLabel.find('.prices').prepend( cell );
		    		};
		    	}else{
					$listLabel.find('.prices > span:eq(0) strong').html( "$ " + min_price );
					$listLabel.find('.prices > span:eq(1) strong').html( "$ " + max_price );		    		
		    	}



		    }
		}

		$('.options strong').html( count );
	},
	filterList: function(){
		var self = this;
		var count = 0;
		for (prop in this.list) {
		    if (this.list.hasOwnProperty(prop)) {

		    	var sub_count = 0;
				for (var i = this.list[prop].length - 1; i >= 0; i--) {

					var on = true;
					if( this.list[prop][i].price_low < this.filter.price_low ||  this.list[prop][i].price_high > this.filter.price_high ){
						on = false;
					}else{	
						on = true;
					}

					if( on &&
							( 
								( !Array.isArray( this.filter.rooms ) && this.filter.rooms !== this.list[prop][i].id ) || 
								( typeof this.filter.rooms !== 'string' && this.filter.rooms.length > 1 && this.filter.rooms.indexOf( this.list[prop][i].id ) == -1 )
							)
						){
							on = false;
					}

					if( on && ( this.filter.arrival || this.filter.departure ) && this.list[prop][i].$elList.find('td.atRange').length > 0 && this.list[prop][i].$elList.find('td.atRange').not('.off').length == 0 ){				
						on = false;
					};	/**/

		 			if( on ){
						$('.features .check-custom:checked').each(function( k, e ){
							if( ( self.list[prop][i].room_features || '' ).indexOf( $(this).val() ) == -1 ){
								on = false;
							}
						}); 				
		 			}

		 			/*
		 			if( on ){
		 				var room_occupancy = $('.filter [name=guests]').val();
		 				if( room_occupancy != undefined && ( self.list[prop][i].room_occupancy || '' ) < room_occupancy ) on = false;
		 			}*/

		 			/*
		 			if( on ){
		 				var beds = $('.filter [name=beds]').val();
		 				if( beds != undefined && ( self.list[prop][i].beds || 1 ) < beds ) on = false;
		 			}*/

		 			if( on ){
		 				/*
		 				if( $("#more_beds input:checked").length ){
							if( !$("[name=bed-for-2]").prop('checked') 
								&& ( self.list[prop][i].inconvenience || "" ) == null )  on = false;

							if( !$("[name=bed-some]").prop('checked') 
								&& ( self.list[prop][i].inconvenience || "" ).toLowerCase() == "some inconvenience" )  on = false;	

							if( !$("[name=bed-greater]").prop('checked') 
								&& ( ( self.list[prop][i].inconvenience || "" ).toLowerCase() == "greater inconvenience" 
								  || ( self.list[prop][i].inconvenience || "" ).toLowerCase() == "some inconvenience" ) ) on = false;
		 				} 	*/
		 				if( $('.filter [name=beds]').val() > 1 && $("#more_beds input:checked").length ){

			 				var convenience = ( self.list[prop][i].inconvenience || "" ).toLowerCase();
			 				if( convenience == "" ){ on = false; }

			 				if( ( convenience.indexOf("basic") > -1 || self.list[prop][i].beds == 2 )
			 					&& !$("#more_beds [name=bed-for-2]").prop('checked') ){ on = false; }

			 				if( convenience.indexOf("some") > -1
			 					&& !$("#more_beds [name=bed-some]").prop('checked') ){ on = false; }

			 				if( convenience.indexOf("greater") > -1
			 					&& !$("#more_beds [name=bed-greater]").prop('checked') ){ on = false; }

		 				} 
		 			}

					this.list[prop][i].$elList.toggleClass( 'filtered', !on );
					this.list[prop][i].$elGrid.toggleClass( 'filtered', !on );
					if( on ) count++;
					if( on ) sub_count++;

				};
				$( "#" + prop + " " ).toggle( ( sub_count > 0) );
				$( "#" + prop + " " ).attr({'data-count': sub_count });
				$( "#" + prop + " span:eq(0) i" ).html( sub_count + " " + (this.list[prop].length>1? t.g('rooms') : t.g('room')) );

			}
		};
		$('.options strong').html( count );
	},
	stringToDate: function( string ){
		var timeArray = string.split('/');
		if( timeArray.length < 3 ){ return false; }
		return new Date(  timeArray[2],  timeArray[1]-1,  timeArray[0] );
	},
	mountCalendar: function( checkIn, checkOut ){

		if( !checkIn && ( checkIn = $('#date-init').val() ) && checkIn == undefined ){ return; }
		if( !checkOut && ( checkOut = $('#date-finish').val() ) && checkOut == undefined ){ return; }

		var self = this;

		$('.rooms button, .tab-price button').addClass('disabled');
		$('.calendar').removeClass('hide').next().removeClass('hide');
		$('.calendar div').not('.loading').removeClass('hide');
		$('.calendar table').addClass('hide');
		$('.calendar .loading').addClass('hide');

		startTime = self.stringToDate( checkIn );
		if( !startTime ) return;

		if( !checkOut ){ 
			endTime = startTime;
		}else{
			endTime = self.stringToDate( checkOut );
			if( !endTime ) return;
		}

		if( startTime > endTime ){ endTime = startTime; }

		for (prop in this.list) {
		    if (this.list.hasOwnProperty(prop)) {
				for (var i = this.list[prop].length - 1; i >= 0; i--) {

					var today = new Date();

					var keyTime = new Date( startTime );

					var week = keyTime.getDay();
					keyTime.setDate( keyTime.getDate() - week - 1 );

					// LIST
					var cell =  this.list[prop][i].$elList.children('span.calendar');
					var label = cell.children('div').addClass('hide');
					var table = cell.find('table');
					if( (table).length == 0 ) return;

					// GRID
					var label = this.list[prop][i].$elGrid.find('.alert').addClass('hide');
					var grid  = this.list[prop][i].$elGrid.find('table');

					var atRange = false;
					var main_price_type;
					this.list[prop][i].$elList.find("td").each(function(j,el){
						var off = false;
						var td = $(this).add( self.list[prop][i].$elGrid.find('td:eq('+j+')') );

						var price = 0;

						var day = keyTime.setDate( keyTime.getDate() + 1 );				

						td.html( keyTime.getDate() );
						td.removeClass('off selected otherMonth atRange');

						var day_data;
						if( self.list[prop][i].days[ keyTime.getDate() + "-" + (keyTime.getMonth()+1) + "-" + keyTime.getFullYear() ] ){
							day_data = self.list[prop][i].days[ keyTime.getDate() + "-" + (keyTime.getMonth()+1) + "-" + keyTime.getFullYear() ];
							price = day_data.price;
							//td.attr('title', day_data.caption || keyTime.toLocaleDateString() );

							td.css('background', '#' + day_data.color_unselected );
						}else{
							day_data = {
								color_unselected: "ccc",
								color_selected: "ddd",
								fecha: true,
								caption:"error",
								price: 0
							};
							price = day_data.price;
							//console.log('dia não encontrado',keyTime.getDate() + "-" + (keyTime.getMonth()+1) + "-" + keyTime.getFullYear(), self.list[prop][i].days );
						}

						td.data('data', {date:keyTime.getTime(), price: price, color: day_data.color_unselected, select_color: day_data.color_selected });		

						if( day_data && day_data.fecha ){
							td.addClass('off'); off = true;
						}

						if( !off && cart.getRoomsDates( self.list[prop][i].id ).indexOf( keyTime.getTime() ) !== -1  ){
							td.addClass('selected');
							td.css('background', '#' + day_data.color_selected );
						}

						if( startTime.getDate() == keyTime.getDate() && startTime.getMonth() == keyTime.getMonth() ){ atRange = true; }
						if( !off && startTime.getMonth() !== keyTime.getMonth() ){ td.addClass('otherMonth'); }
						if( !off && atRange ){ /*td.addClass('selected') /*auto-select*//* */;}
						if( atRange ){td.addClass('atRange'); }
						if( endTime.getDate() == keyTime.getDate() && endTime.getMonth() == keyTime.getMonth() && atRange){ atRange = false;	}

				
						if( off ) td.html('X');
					});

					if( table.find('.atRange.off').length == 0 ){ table.find('.atRange').addClass('allEnabled'); }
					if( grid.find('.atRange.off').length == 0 ){ grid.find('.atRange').addClass('allEnabled'); }

					table.removeClass('hide');
					grid.removeClass('hide');

				};
			};
		};

		$('ul.body').addClass('open-calendar');

		//});
	},
	mountRoom: function( type, caller ){
		console.log( type, caller );

		caller.addClass('open');

		if( $('.room_content').data('url') == caller.find('a').last().attr('href') ){
			$('.room_content .close').click();
			return false;
		}else{
			$('.room_content .close').click();
		}

		var $close = $('<a class="close" href="#close"><i class="room_close"></i></a>');
		var url = "";
		$close.click(function(){
			caller.removeClass('open');
			$room.slideUp(500,function(){ $(this).remove(); });
			return false;
		});
		caller.siblings('.room-info').remove();		
		if( type == "list" ){
			url = caller.find('a').attr('href');	
			var $room = $('<li class="room_content room"><div class="loading">' + t.g('Loading...') + '</div></li>');		
			$room.data('url', url);
			$room.insertAfter( caller );
			
		}else{
			url = caller.find('a').last().attr('href');	
			var $room = $('<li class="room_content room"><div class="loading">' + t.g('Loading...') + '</div></li>');		
			$room.data('url', url);
			$next = caller;
			while( $next.next().length && Math.abs($next.next().position().top - caller.position().top) < 4 ){
				$next = $next.next();
			}

			$room.insertAfter( $next );
		}

		$.ajax({
			url: url,
			success: function(data){
				var room_content = $(data).find('.inside').html();
				room_content = '<div class="content">'
								+'<div class="inside content_block">'
								+ room_content
								+'</div>'
								+'</div>';								
				$room.hide();	
				$room.html( room_content );
				//$room.find('.must_see').after( '' );				
				$room.find('.other_rooms, .footer').remove();					
				$room.prepend( $close );
				$room.slideDown( 500, function(){ 
					$room.find('[data-propresize="true"]').propResize();
					$room.find("[data-active=slider]").slideshow();					
				});	


				$('html, body').animate({
	                scrollTop: $room.offset().top - ($(window).width() < 720 ? $("#navigation").outerHeight():0) - $('.tabpanel .fixer').outerHeight()
	            }, 500);					
			},
			error: function(){
				$room.remove();
			}
		});
	}
}


$(function(){
	
	$('.nav-tabs a').click(function(){
		var t = $(this).attr('href');
		$(this).parent().addClass('active');
		if( t ){
			$( t ).show();

			$( this ).parent().siblings().each(function( i, e ){
				var k = $( e ).find('a').attr('href');
				if( t == k ) return;
				$( e ).removeClass('active');
				$( k ).hide();
			});
		};
		return false;
	});

	/* ==================== SLIDERS */

	var slider = {
		min: 0,
		max: 100
	}

	var barra = $('.bar');
	var subbar = $('.sub_bar');
	var tabs = $('.bar div');
	var currentTab = false;

	tabs.on('mousedown',function(e){
		currentTab = $(this);
		e.stopPropagation();
		return false;
	});

	barra.on('mousemove', function( e ){
		//console.log(e.currentTarget);
		if( e.buttons && e.target == barra[0] ) updateTab( e );
		e.stopPropagation();
		return false;
	});

	barra.on('click', function( e ){
		//console.log(e.currentTarget);
		if( e.target == barra[0] ) updateTab( e );
		e.stopPropagation();
		return false;
	});

	subbar.on('mousemove',function(e){
		//console.log(e.currentTarget);
		if( e.buttons && e.target == subbar[0] ) updateTab( e.offsetX + subbar.position().left );
		e.stopPropagation();
		return false;
	});

	subbar.on('click', function( e ){
		//console.log(e.currentTarget);
		if( e.target == subbar[0] ) updateTab( e.offsetX + subbar.position().left );
		e.stopPropagation();
		return false;
	});

	function updateTab( e, forcePercent ){
		var larg = barra.width();

		if( currentTab && e ){
			var pos = Math.min( Math.max( 10, ( e.offsetX || e ) ), larg + 10 ) - 10;
			var posPer = forcePercent == undefined ? pos/larg : forcePercent;

			currentTab.html( "$" + Math.floor( Math.floor( posPer * 100 ) * 6 ) );
			if( currentTab.hasClass('minimo') ){ slider.min = posPer ; $("#price-start").val( Math.floor( posPer * 600 ) ? Math.floor( posPer * 600 ) : 0 ); }
			if( currentTab.hasClass('maximo') ){ slider.max = posPer ; $("#price-end"  ).val( Math.floor( posPer * 600 ) ? Math.floor( posPer * 600 ) : 0 ); }

			console.log({
				posPer: posPer,
				min: slider.min,
				max: slider.max
			});

		}

		tabs.eq(0).css({ left: slider.min * larg  });
		tabs.eq(1).css({ left: slider.max * larg  });

		subbar.css({'left': Math.min( slider.min, slider.max  ) * larg + 10, 'width': ( Math.max( slider.min, slider.max  ) - Math.min( slider.min, slider.max  ) ) * larg });

		lists.filter.price_low =  $("#price-start").val() || 1 ;
		lists.filter.price_high = $("#price-end"  ).val() || 600;
		lists.filterList();
	}

	currentTab = tabs.eq(0);
	updateTab(0, 0);
	currentTab = tabs.eq(1);
	updateTab(600, 1);
	currentTab = false;
	$(window).resize( function(){ updateTab(); } );

	tooltip.init();

	/* =============== END SLIDERS */


	/*$(".calendar_price td").click(function(){
		if( !$(this).hasClass("off") ){ $(this).toggleClass("selected"); }
	});*/
	lists.init();
});

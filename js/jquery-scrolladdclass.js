﻿;(function ( $, window, document, undefined ) {

    
    var pluginName = 'scrollAddClass',

        defaults = {
            
            class: 'viewed',
            viewingClass: 'now-viewing',
            //futureClass: 'not-yet-viewed',
            //pastClass: 'not-more-viewed',

        };



    // The actual plugin constructor

    function Plugin( element, options ) {

        this.element = element;

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;

        this._name = pluginName;


        this.init();

    }



    Plugin.prototype.init = function () {
    	var self = this;
        var scrollId, scrollCount = 0;

    	$(window).scroll(function(){    
            if(scrollId && scrollCount < 20){
                clearTimeout(scrollId);
            }
            scrollCount++;

            var newViewed = [];
            var actualViewd = [];

            scrollId = setTimeout(function(){
                scrollCount = 0;

                var wPos = {
                    top: $(window).scrollTop(),
                    middle: $(window).scrollTop() + $(window).height() / 2,
                    bottom: $(window).scrollTop() + $(window).height()
                };

                $('body').toggleClass('at-top', ( wPos.top < 10 ) );

    	    	$(self.element).each(function(){
    	    		//if( $(this).hasClass( self.options.class ) ) return;

                    var tPos = {
                        top: $(this).offset().top,
                        middle: $(this).offset().top + $(this).height() / 2,
                        bottom: $(this).offset().top + $(this).height()
                    }

                    newViewed = [];
                    actualViewed = [];

    	    		if( tPos.top < wPos.bottom ){
                        if( !$(this).hasClass( self.options.class ) ){
                            $(this).trigger('new-viewed');
                            $(this).addClass( self.options.class );
                        }  	                     
    	    		}else{
                        //$(this).removeClass( self.options.pastClass );                        
                    }

                    if( ( tPos.top < wPos.top && tPos.bottom > wPos.bottom ) ||
                        ( tPos.top > wPos.top && tPos.top < wPos.bottom ) ||
                        ( tPos.bottom > wPos.top && tPos.bottom < wPos.bottom )
                    ){
                        $(this).addClass( self.options.viewingClass );
                        //console.log( this, wPos, tPos );
                        //$(this).removeClass( self.options.pastClass ); 
                        //$(this).removeClass( self.options.futureClass );  
                    }else{
                        $(this).removeClass( self.options.viewingClass );
                    }

                    

                    // if( bottomThis > topOfWindow ){
                    //     $(this).addClass( self.options.futureClass );
                    // }else{
                    //     $(this).removeClass( self.options.futureClass );                        
                    // }
    	    	});

                if( newViewed.length > 0 ){ $(window).trigger('new-viewed', newViewed); }
                if( actualViewed.length > 0 ){ $(window).trigger('views-update', actualViewed); }

            }, 30);
		}).scroll();
    };


    Plugin.prototype.review = function( targets ){

        // var bottomOfWindow = $(window).scrollTop() + $(window).height();

        // $(self.element).each(function(){
        //     if( $(this).hasClass( self.options.class ) ) return;
        //     var tPos.top = $(this).offset().top;

        //     if( tPos.top < bottomOfWindow ){
        //         $(this).addClass( self.options.class );
        //     }

        // });

    };


    // A really lightweight plugin wrapper around the constructor, 

    // preventing against multiple instantiations

    $.fn[pluginName] = function ( options ) {

        return this.each(function () {

            if (!$.data(this, 'plugin_' + pluginName)) {

                $.data(this, 'plugin_' + pluginName, 

                new Plugin( this, options ));

            }

        });

    }



})( jQuery, window, document );

$(function(){

	var id = window.location.hash.replace('#','');

	if(id){
		$.ajax({
			url: config.serverUrl + "client/arrival/" + id,
			method: "POST",			
			success: function( res ){
				if( res.result == 'success' ){
					$('.name p').html( res.data.client.name.trim() + " " + res.data.client.last_name.trim() );
					$('h2 span').html( res.data.client.name.trim() + " " + res.data.client.last_name.trim() );
				}else{
					window.location = "./index.html";
				}
				console.log( res.data );
			},
			error: function(){
				window.location = "./index.html";
			}
		});
	}else{
		window.location = "./index.html";
	}

	/*
	 *	Arrival form checkbox first screen
	 */
	$('form label.check-custom').click(function() {
		$('div[rel='+$(this).attr('for')+']').toggle(this.checked);

		$( $( '[name=' + $( '#' + $(this).attr('for') ).attr('name') + ']:checked' ).not( $( '#' + $(this).attr('for') ) ) ).click();
	});

	/*
	 *	Arrival next screen
	 */
	$('form .nextScreen').click(function() {
		event.preventDefault();
		$('.first_screen').hide();
		$('.second_screen').show();
		$('html, body').animate({ scrollTop: $(".second_screen h1").offset().top }, 500);
		return false;
	});

	$('.second_screen form button').click(function(){

		var arrial = {};
		/*$('form').find('input, select, textarea').each(function(){
			arrial[ $(this).attr('name') ] = $(this).attr('type') == 'checkbox' ? $(this).prop('checked') : $(this).val();
		});*/
		
		arrial['heard_by'] = $('[name=heard_by]:checked').val();
		arrial['decide_by'] = $('[name=decide_by]:checked').val();
		arrial['juice'] = $('[name=juice]:checked').val();
		arrial['margarita'] = $('[name=margarita]:checked').val();
		arrial['beer'] = $('[name=beer]:checked').val();
		arrial['comments'] = $('.arrival_infos [name=comments]').val();

		arrial['email'] = $('.arrival_infos [name=email]').val();
		arrial['first_name'] = $('.arrival_infos [name=first_name]').val();
		arrial['last_name'] = $('.arrival_infos [name=last_name]').val();
		arrial['country'] = $('.arrival_infos [name=country]').val();
		arrial['emergency'] = $('.arrival_infos [name=emergency]').val();
		arrial['allergies'] = $('.arrival_infos [name=allergies]').val();

		arrial['flight_date'] = $('[name=flight_date]').val();
		arrial['flight_airport'] = $('[name=flight_airport]').val();
		arrial['flight_airline'] = $('[name=flight_airline]').val();
		arrial['arrival_time'] = $('[name=arrival_time]').val();

		arrial['comeby'] = $('[name=comeby]:checked').val();

		console.log( arrial );

		if(id){
			$.ajax({
				url: config.serverUrl + "client/arrival/" + id + "/send",
				data: arrial,
				method: "POST",			
				success: function( data ){
					console.log( data );
				},
			});
		}

		return false;
	});

});
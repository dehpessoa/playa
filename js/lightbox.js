var Lightbox = function (){
	var self = this;

	this.links = [];
	this.currentPage = 1;
	this.$el;

	this.init = function(){
		this.$el = $('<div class="lightbox" >'+
						'<div class="close"></div>'+			
						'<div class="controle_esq"></div>'+
						'<div class="miolo">'+
							'<ul class="thumbs">'+
								'<li></li>'+
								'<li></li>'+
								'<li></li>'+
								'<li></li>'+
							'</ul>'+
							'<div class="canvas"></div>'+
							'<div class="pagination"><strong></strong> / <span></span></div>'+
						'</div>	'+
						'<div class="controle_dir"></div>'+
					'</div>');
		$('body').append( self.$el );

		this.$el.on('click','.close',function(e){
			self.deactive();
			e.stopPropagation();
		});

		this.$el.on('click','.controle_esq',function(e){
			self.prev();
			e.stopPropagation();
		});

		this.$el.on('click','.controle_dir',function(e){
			self.next();
			e.stopPropagation();
		});

		this.scan( $('body') );
	};

	this.scan = function( base ){
		base.find('[data-active=lightbox]').each(function(){
			$(this).click(function(){
				var list = $(this);
				var rel = $(this).attr('rel');
				var group = $('[rel='+rel+']').not($(this));
				list = $.merge(list,group);

				self.update( list );
				return false;
			});
		});
	};

	this.update = function( children ){
		( children || ( children = $() ) );
		this.$el.find(".thumbs").empty();

		if( children.length == 1){
			this.$el.find('.controle_esq').add('.controle_dir').addClass('disabled');
		}else{
			this.$el.find('.controle_esq').addClass('disabled');
			this.$el.find('.controle_dir').removeClass('disabled');
		};

		this.$el.find('.pagination strong').text( '01' );
		this.$el.find('.pagination span').text( children.length < 10 ? "0" + children.length : children.length );

		self.links = [];
		children.each(function( i, e ){
			var link = $("<li></li>");

			var cover = $(this).data('icon') || $(this).attr('href');
			var content = $(this).attr('href');

			if( !content ) return;

			self.links.push( content );

			link.css( 'backgroundImage', "url(" + cover + ")" );

			link.click(function(){
				self.openContent( i );
				console.log("showContent");
			});
			self.$el.find(".thumbs").append( link );			
		});

		self.active();	
		self.currentPage = 0;
		self.openContent(0);
	};

	this.next = function(){
		if( self.currentPage < self.links.length - 1 ){
			self.currentPage++;
			self.openContent( self.currentPage );
		}
	};

	this.prev = function(){
		if( self.currentPage > 0 ){
			self.currentPage--;
			self.openContent( self.currentPage );
		}
	};

	this.openContent = function( id ){
		var url = self.links[id];
		self.currentPage = id;

		this.$el.find('.controle_esq').toggleClass('disabled', ( self.currentPage == 0 ));
		this.$el.find('.controle_dir').toggleClass('disabled', ( self.currentPage == self.links.length - 1 ));

		this.$el.find('.pagination strong').text( id < 9 ? "0" + ( id + 1 ) : ( id + 1 ) );

		self.$el.find('.canvas').children().remove();

		var ext = url.split('.').pop().split("?").shift();

		var newElem;
		if( ext == 'jpg' || ext == 'png' ){
			newElem = $("<img />");
			newElem.css({opacity:0});
			newElem.attr('src', url );
		}else{
			newElem = $('<video class="" width="auto" height="100%" autoplay="" controls>'+
				'<source type="video/mp4">'+
				'Your browser does not support the video tag.'+
			'</video>');
			newElem.find('source').attr('src', url );
		}

		newElem.on("load", function() {
			self.resizeContent( newElem );
			newElem.animate({opacity:1}, 150);
			console.log( this, $(this).width(), $(this).height() );
		}).each(function() {
			if(this.complete) $(this).load();
		});	
		$( window ).resize( function(){ self.resizeContent( newElem ) } );	

		self.$el.find('.canvas').append( newElem );
	};

	this.resizeContent = function( elem ){
		var lProp = self.$el.find('.canvas').height() / self.$el.find('.canvas').width();
		var iProp = elem.height() / elem.width();

		if( lProp < iProp ){
			elem.css({marginTop:0});
			elem.height( self.$el.find('.canvas').height() );
			elem.width( self.$el.find('.canvas').height() / iProp );
		}else{
			var h = self.$el.find('.canvas').width() * iProp;
			elem.css({marginTop:( self.$el.find('.canvas').height() - h )/2});			
			elem.height( h );
			elem.width( self.$el.find('.canvas').width() );
		};
	};


	this.active = function(){
		self.$el.find('.canvas').children().remove();
		this.$el.addClass('active');
	};

	this.deactive = function(){
		self.$el.find('.canvas').children().remove();
		this.$el.removeClass('active');
	};

	this.init();
};

var Slider = function(){
	var self = this;

	$.fn.slideshow = function( options ){
		var $canvas = $(this);	
		if( !$canvas.length ) return false;	
		var pos = 0;
		var key = $canvas.attr('rel');
		var list = [];

		var $video_control = $('<a class="play_button big" href="#"></a>');
		var $nav = $('<nav class="slider-control"><button class="prev"></button><button class="next"></button></nav>');

		$nav.find('.next').click(function(){			
			pos = ( pos < list.length - 1 )? pos + 1 : 0 ;
			self.updateCanvas( $canvas, list[ pos ] );

			//$(this).toggleClass('disabled', ( pos == list.length - 1 ));
			//$nav.find('.prev').removeClass('disabled');
		});		

		$nav.find('.prev').click(function(){
			pos = ( pos > 0 )? pos - 1 : list.length - 1 ;
			self.updateCanvas( $canvas, list[ pos ] );

			//$(this).toggleClass('disabled', ( pos == 0 ));
			//$nav.find('.next').removeClass('disabled');
		});	

		$('[rel='+key+']').not(this).each(function(){
			list.push( $(this) );
		});

		$canvas.find('.play_button').remove();
		$canvas.append( $video_control );
		if( list.length > 1 )  $canvas.append($nav);
		
		self.updateCanvas( $canvas, list[ 0 ] );
	}

	this.updateCanvas = function( $canvas, url ){

		var $img_loader = $("<img>");

		$canvas.find('video').remove();
		$canvas.find('.bg').css({opacity:0});

		var ext = url.attr('href').split('.').pop().split("?").shift();
		if( ext == 'jpg' || ext == 'png' ){
			$canvas.find('.play_button').addClass('hide');			
			$canvas.find('.bg').css({'backgroundImage':'url('+url.attr('href')+')'});

			$img_loader.load(function(){ $canvas.find('.bg').animate({opacity:1}, 300);	});
			$img_loader.attr('src', url.attr('href'));
		}else{
			newElem = $('<video class="hide" width="auto" height="100%">'+
				'<source type="video/mp4">'+
				'Your browser does not support the video tag.'+
			'</video>');
			$canvas.append( newElem );

			$canvas.find('.bg').css({'backgroundImage':'url('+ url.data('cover') +')'});
			$canvas.find('.play_button').removeClass('hide');
			$canvas.find('.play_button').unbind('click').click(function(){
				newElem.removeClass('hide').css({opacity:1});
				newElem[0].setAttribute("controls","controls");
				newElem[0].play();
				$(this).addClass('hide');

				return false;
			});				
			newElem.find('source').attr('src', url.attr('href') );
			newElem[0].onended = function() {
				$canvas.find('.play_button').removeClass('hide');
				newElem.animate({opacity:0}, 300, function(){ newElem.addClass('hide'); });
			};

			$img_loader.load(function(){ $canvas.find('.bg').animate({opacity:0.3}, 300);	});
			$img_loader.attr('src', url.data('cover'));
		}
		
	}

	this.init = function(){
		$("[data-active=slider]").each(function(){
			$(this).slideshow();
		});
	};

	

	this.init();
};

var lightbox;
$(function(){
	lightbox = new Lightbox();
	slider = new Slider();
});
var lists = {
	urlList: 'http://projetos.thbastos.com/topogigio/rooms/list/all',
	$elList: false,
	$elGrid: false,
	list: [],
	con: false,
	filter:{
		low_price: 0,
		high_price: 1000
	},
	init: function(){
		var self = this;

		$( "#date-init, #date-finish" ).datepicker({
			inline: true,
			"dateFormat": "dd/mm/yy",
			minDate: ( new Date() ),
		});

		$('.select-style select').on('focus',function(){ $(this).parent().addClass('focus'); });
		$('.select-style select').on('blur',function(){ $(this).parent().removeClass('focus'); });

		$('#date-finish').change(function(){
			self.updateList( function(){
				self.printList();
				self.mountCalendar( $('#date-init').val(), $('#date-finish').val() );
				self.filterList();
			},$('#date-init').val(), $('#date-finish').val());			
		});

		$('#date-init').change(function(e){
			console.log(e);
			if( !$('#date-finish').val() ){
				var tomorrow = new Date( $('#date-init').datepicker('getDate') );
				tomorrow.setDate( tomorrow.getDate() + 1 );
				$('#date-finish').datepicker("setDate", tomorrow );
			}
			$('#date-finish').change();
		});

		/* ================ LIST */
		$('[name="check-dates"], .check-custom, .filter [name=area], .filter [name=beds]').change(function(){
			self.filterList();
		});

		this.updateList();		
	},
	addRoom: function( room ){
		( room || ( room = {
			price_low: ( Math.floor( 25 * Math.random() ) * 10 ),
			price_high: ( Math.floor( 50 * Math.random() ) * 20 ),
			name: ( "Room name " + ( Math.floor( Math.random() * 1000 ) ) ),
			area: "Playa",
			description: "Canopy King bed, Private Deck, Lounge chairs, hammock, panorama ocean view, wifi, privacy and comfort.",
			img: "http://projetos.thbastos.com/playa/content/price/thumb_01.png",
			url: "www.google.com",
			beds: ( 1 + Math.floor( 3 * Math.random() ) ),
			area: (['beach front','ocean view','garden view'])[( Math.floor( Math.random() * 3 ) )],
			features:( function(){
					var ret = [];
					if( Math.random() > 0.5 ) ret.push('wifi');
					if( Math.random() > 0.5 ) ret.push('tv');
					if( Math.random() > 0.5 ) ret.push('hottube');
					if( Math.random() > 0.5 ) ret.push('sp4');
					return ret;
				})()
		} ) );

		room.$elList = false;
		room.$elGrid = false;

		this.list.push( room );
	},
	resetList: function(){
		for (var i = this.list.length - 1; i >= 0; i--) {
			if( this.list[i].$elList ) this.list[i].$elList.remove();  
			if( this.list[i].$elGrid ) this.list[i].$elGrid.remove();  
		}
		this.list = [];
	},
	updateList: function( callback, startInput, endInput ){
		var self = this;
		/*for (var i = list.length - 1; i >= 0; i--) {
			this.addRoom( list[i] );
		};*/
		var start, end;
		start = end = '';
		if( startInput ){
			var startDate = self.stringToDate(startInput);
			startDate.setDate( startDate.getDate() - 15 );
			start = "/" + startDate.getFullYear() + "-" + (startDate.getMonth()+1) + "-" + startDate.getDate();
		}

		if( endInput || (endInput = startInput) ){
			var endDate = self.stringToDate(endInput);
			endDate.setDate( endDate.getDate() + 15 );
			end = "/" + endDate.getFullYear() + "-" + (endDate.getMonth()+1) + "-" + endDate.getDate();
		}

		if( self.con ){ self.con.abort(); self.con = false; }
		self.con = $.ajax({
			url: this.urlList + start + end,
			dataType: "json",
			cache: false,
			beforeSend: function(){
				var calendars = $('.calendar');
				calendars.find('div').addClass('hide');
				calendars.find('table').addClass('hide');
				calendars.find('div.loading').removeClass('hide');
			},
			success: function( res ){
				self.con = false;
				//res = JSON.parse( data );
				self.resetList();

				if( res.result ){
					for (var i = res.data.length-1; i >= 0; i--) {
						if( res.data[i] == undefined ){
							console.log( i, res.data[i] );
							continue;
						}
						var room = {
							id: res.data[i].id_casa,
							name: res.data[i].casa,
							area: res.data[i].location,
							description: res.data[i].description,
							price_low: parseFloat( res.data[i].summer_price ),
							price_high: parseFloat( res.data[i].winter_price ),
							price_holiday: parseFloat( res.data[i].holidays_price ),
							reserved: res.data[i].dates,
							img: 'rooms/images/'  + res.data[i].slug + '/thumb/'  + res.data[i].slug + '_01.jpg',
							url: res.data[i].slug + '.html',
							video: res.data[i].video,
							inconvenience: res.data[i].inconvenience,
							beds: res.data[i].bednote,
							features: res.data[i].features
						};
						self.addRoom( room );
					};
				}
				if( callback ){
					callback();
				}else{
					self.printList();
				}
				
			}
		});



		
		$('.options strong').html( this.list.length );
	},
	printList: function(){
		var self = this;
		for (var i = this.list.length - 1; i >= 0; i--) {

			this.list[i].$elList = $(''+
					'<li>'+
						'<span class="calendar">'+
						'	<div class="">Please select your check <br>in date the upper menu</div>'+
						'	<div class="loading hide">Loading...</div>'+
						'</span>'+
						'<span class="options">'+
						'	<div class="video">'+
						'		<span><a data-active="lightbox"><i class="video"></i></a></span>'+
						'	</div>'+
						'	<div>'+
						'		<button class="disabled">add to cart</button>'+
						'	</div>'+
						'</span>'+
					'</li>'
				);

			this.list[i].$elGrid = $(''+
					'<li> ' +
					'	<div class="thumbs"> ' +
					'		<img src="http://projetos.thbastos.com/playa/content/price/thumb_01.png"> ' +
					'		<i class="ic_price_room ic_search"></i> ' +
					'		<a ' +
					'			href="#" '+
					'			data-icon="#"'+
					'			data-active="lightbox" '+
					'			style="background:url("http://projetos.thbastos.com/playa/content/home/hotel_areas_01.png");" >'+
					'				<i class="play_button big"></i> ' +					
					'		</a> ' +					
					'		<span>$ 375</span> ' +					
					'	</div> ' +
					'	<div class="info"> ' +
					'		<a href="#"> ' +
					'			<h1>Ixchel Penthouse</h1> ' +
					'			<p>Canopy King bed, Private Deck, Lounge chairs, hammock, panorama ocean view, wifi, privacy and comfort</p> ' +
					'		</a> ' +
					'		<span class="calendar">								 ' +
					'			<div class="alert"> ' +
					'				<hr> ' +
					'				<p class="warn">Please select your check in date in the upper menu</p> ' +
					'				<hr> ' +
					'			</div> ' +
					'			<div class="loading hide">'+
					'				<hr> ' +
					'				<p class="warn">Loading...</p> ' +
					'				<hr> ' +
					'			</div>'+
					'		</span> ' +
					'		<button class="disabled">Add to cart</button> ' +
					'	</div> ' +
					'</li> '
				);

			$name = $('<span><a href="#"><strong></strong><div></div></a></span>');
			$price_low =  $('<div class="price_low"><small>May to oct</small><strong></strong></div>');
			$price_high = $('<div class="price_high"><small>Nov to Aprill</small><strong></strong></div>');

			$table = $('<table class="calendar calendar_price hide"></table>');
			$thead = $('<tr/>');

			for (var k = (['Su','Mo','Tu','We','Th','Fr','Sa']).length - 1; k >= 0; k--) {
				$thead.append( $('<th/>').html( (['Su','Mo','Tu','We','Th','Fr','Sa'])[k] ) );
			};
			
			for (var k = 4; k >= 0; k--) {
				var $line = $('<tr/>');
				for (var j = 6; j >= 0; j--) {
					var $cell = $('<td></td>');
					$cell.html( k * 7 + j );
					$cell.click(function(){
						if( !$(this).hasClass("off") ){ $(this).toggleClass("selected"); }
					});	

					$line.prepend( $cell );
				};
				$table.prepend( $line );
			};
			$table.prepend( $thead );

			this.list[i].$elList.find('.options button').click(function(){ 
				var dates = [];
				var room = $(this).parent().parent().parent().data('room');

				if( room.$elList.find('.selected').length < 1 ) return;

				var price = 0;
				room.$elList.find('.selected').each(function(){
					price += $(this).data('price') || 0;
					dates.push( $(this).data('date') );
				});

				var children = [];
				if( $("[name=check-children-alert]").prop('checked') && $('[name="child_age_1"]').val() ) children.push( $('[name="child_age_1"]').val() );
				if( $("[name=check-children-alert]").prop('checked') && $('[name="child_age_2"]').val() ) children.push( $('[name="child_age_2"]').val() );

				cart.addRoom({
					id: 	room.id, 
					img:    room.img,
					name:   room.name,
					nights: dates.length,
					value:  price,
					guests: $('.filter [name=guests]').val(),
					beds:   $('.filter [name=beds]').val(),
					children: children,
					days: dates
				});
			});

			this.list[i].$elGrid.find('button').click(function(){ 
				var dates = [];
				var room = $(this).parent().parent().data('room');

				if( room.$elGrid.find('.selected').length < 1 ) return;

				var price = 0;
				room.$elGrid.find('.selected').each(function(){
					price += $(this).data('price') || 0;
					dates.push( $(this).data('date') );
				});

				var children = [];
				if( $("[name=check-children-alert]").prop('checked') && $('[name="child_age_1"]').val() ) children.push( $('[name="child_age_1"]').val() );
				if( $("[name=check-children-alert]").prop('checked') && $('[name="child_age_2"]').val() ) children.push( $('[name="child_age_2"]').val() );

				cart.addRoom({
					id: 	room.id, 
					img:    room.img,
					name:   room.name,
					nights: dates.length,
					value:  price,
					guests: $('.filter [name=guests]').val(),
					beds:   $('.filter [name=beds]').val(),
					children: children,
					days: dates
				});
			});

			/*  ==========  */

			$name.find('strong').html( this.list[i].name );
			this.list[i].$elGrid.find('h1').html( this.list[i].name );

			this.list[i].$elGrid.find('p:eq(0)').html( this.list[i].description );

			$name.find('div').html( this.list[i].area );
			$name.find('a').attr('href', this.list[i].url ).click( function(){
				self.mountRoom( 'list', $(this).parent().parent() );
				return false;
			});
			$price_low.find('strong').html('$ '  + this.list[i].price_low );
			$price_high.find('strong').html('$ ' + this.list[i].price_high );

			this.list[i].$elList.find('.options a:eq(1)').attr('href', this.list[i].url );

			this.list[i].$elGrid.find('.thumbs img').attr('src', this.list[i].img );

			if( this.list[i].video ){
				this.list[i].$elList.find('.options a:eq(0)').attr('href', this.list[i].video );
				this.list[i].$elList.find('.options a:eq(0)').attr('data-icon', this.list[i].img );

				this.list[i].$elGrid.find('.thumbs a').attr('href', this.list[i].video );
				this.list[i].$elGrid.find('.thumbs a').attr('data-icon', this.list[i].img );				
			}else{
				this.list[i].$elList.find('.options span').addClass('disabled');
				this.list[i].$elList.find('.options a:eq(0)').removeAttr('data-active');

				this.list[i].$elGrid.find('.thumbs a').addClass('hide');
			}

			this.list[i].$elGrid.find('.info a').attr('href', this.list[i].url );
			this.list[i].$elGrid.find('div:eq(0) span').html(  '$ ' + this.list[i].price_low +' / $ ' + this.list[i].price_high );

			this.list[i].$elList.find('.calendar').prepend( $table );
			this.list[i].$elGrid.find('.calendar').prepend( $table.clone( true, true ) );

			this.list[i].$elList.data('room', this.list[i]);
			this.list[i].$elGrid.data('room', this.list[i]);
			/*this.list[i].$elList.find("td").click(function(){
				if( !$(this).hasClass("off") ){ $(this).toggleClass("selected"); }
			});	*/		

			this.list[i].$elList.prepend( $('<span/>').append( $price_low ).append( $price_high ) );
			this.list[i].$elList.prepend( $('<span/>').append( $price_low.clone() ) );
			this.list[i].$elList.prepend( $name );

			$('.tab-price ul').append( this.list[i].$elList );
			$('.tab-accommodation ul').append( this.list[i].$elGrid );

			$('.options strong').html( this.list.length );

			if(lightbox) lightbox.scan( this.list[i].$elGrid );
			if(lightbox) lightbox.scan( this.list[i].$elList );
		};
	},
	filterList: function(){
		var self = this;
		var count = 0;
		for (var i = this.list.length - 1; i >= 0; i--) {
			//console.log(this.list[i]);

			var on = true;
			if( this.list[i].price_low < this.filter.price_low ||  this.list[i].price_high > this.filter.price_high ){
				on = false;
			}else{	
				on = true;
			}

			if( on && $('[name="check-dates"]').prop('checked') && this.list[i].$elList.find('td.atRange').not('.off').length == 0 ){				
				on = false;
			};	

 			if( on ){
				$('.features .check-custom:checked').each(function( k, e ){
					//if( $.inArray( $(this).val(), self.list[i].features) == -1 ){ 
					if( ( self.list[i].features || '' ).indexOf( $(this).val() ) == -1 ){
						on = false;
					}
				}); 				
 			}

 			if( on ){
 				var area = ( $('.filter [name=area]').val()).toLowerCase();
 				if( area && area !== self.list[i].area.toLowerCase() ) on = false;
 			}

 			if( on ){
 				var beds = $('.filter [name=beds]').val();
 				if( beds && ( self.list[i].beds || '' ).indexOf( beds ) == -1 ) on = false;
 			}

 			if( on ){
 				if( $("#more_beds input:checked").length ){
					if( !$("[name=bed-for-2]").prop('checked') 
						&& self.list[i].inconvenience == null )  on = false;

					if( !$("[name=bed-some]").prop('checked') 
						&& self.list[i].inconvenience == "Some Inconvenience" )  on = false;	

					if( !$("[name=bed-greater]").prop('checked') 
						&& ( self.list[i].inconvenience == "Greater Inconvenience" 
						  || self.list[i].inconvenience == "Some Inconvenience") )  on = false;
 				} 				
 			}

			this.list[i].$elList.toggle( on );
			this.list[i].$elGrid.toggle( on );
			if( on ) count++;

		};
		$('.options strong').html( count );
	},
	stringToDate: function( string ){
		var timeArray = string.split('/');
		if( timeArray.length < 3 ){ return false; }
		return new Date(  timeArray[2],  timeArray[1]-1,  timeArray[0] );
	},
	mountCalendar: function( checkIn, checkOut ){
		var self = this;

		$('.rooms button, .tab-price button').addClass('disabled');
		$('.calendar div').not('.loading').removeClass('hide');
		$('.calendar table').addClass('hide');
		$('.calendar .loading').addClass('hide');

		startTime = self.stringToDate( checkIn );
		if( !startTime ) return;

		if( !checkOut ){ 
			endTime = startTime;
		}else{
			endTime = self.stringToDate( checkOut );
			if( !endTime ) return;
		}

		if( startTime > endTime ){ endTime = startTime; }
		var middleTime = new Date( ( startTime.getTime(startTime) + endTime.getTime(endTime) ) / 2 );
		var rangeDays   = Math.max( 30, ( endTime - startTime ) / Math.ceil( 24 * 60 * 60 * 1000 ));

		console.log( 'rangeDays', rangeDays );

		for (var i = 0; i < this.list.length; i++) {

			var today = new Date();

			var keyTime = new Date( middleTime );

			var week = keyTime.getDay();
			keyTime.setDate( keyTime.getDate() - week - (  15  ) );

			// LIST
			var cell =  this.list[i].$elList.children('span').eq(3);
			var label = cell.children('div').addClass('hide');
			var table = cell.find('table');
			this.list[i].$elList.find('button').removeClass('disabled');
			if( (table).length == 0 ) return;

			// GRID
			var label = self.list[i].$elGrid.find('.alert').addClass('hide');
			var grid  = self.list[i].$elGrid.find('table');
			this.list[i].$elGrid.find('button').removeClass('disabled');

			var atRange = false;
			this.list[i].$elList.find("td").each(function(j,el){
				var off = false;
				var td = $(this).add( self.list[i].$elGrid.find('td:eq('+j+')') );
				var price = 0;

				var day = keyTime.setDate( keyTime.getDate() + 1 );
				td.data('date', keyTime.getTime());

				td.html( keyTime.getDate() );
				td.removeClass('off selected otherMonth atRange');

				if( keyTime.getMonth() > 3 && keyTime.getMonth() < 10 ){
					price = self.list[i].price_low;
					td.addClass('low-price');
				}else{
					if( keyTime.getMonth() == 11 && keyTime.getDate() > 23 ){
						price = self.list[i].price_holiday;
						td.addClass('high-price');
					}else{
						price = self.list[i].price_high;
						td.addClass('high-price');						
					}
								
				};

				td.data('price', price);				

				if( today >= keyTime ){ td.addClass('off'); off = true; }
				if( !off && self.list[i].reserved.indexOf( keyTime.getDate() + "-" + (keyTime.getMonth()+1) + "-" + keyTime.getFullYear()  ) !== -1 ){ td.addClass('off'); off = true; }

				if( startTime.getDate() == keyTime.getDate() && startTime.getMonth() == keyTime.getMonth() ){ atRange = true;	}
				if( !off && startTime.getMonth() !== keyTime.getMonth() ){ td.addClass('otherMonth');	}
				if( !off && atRange ){ td.addClass('selected');td.addClass('atRange'); }
				if(  endTime.getDate() == keyTime.getDate() && endTime.getMonth() == keyTime.getMonth() && atRange){ atRange = false;	}

				td.attr('title', keyTime.toLocaleDateString() );

			});

			table.removeClass('hide');
			grid.removeClass('hide');

		};

		//});
	},
	mountRoom: function( type, caller ){
		console.log( type, caller );
		if( type == "list" ){
			var $room = $('<li><div class="loading">Loading</div></li>');
			var $close = $('<a href="#close">X</a>');
			$close.click(function(){
				$room.remove();
				return false;
			});
			$room.insertAfter( caller );
			$.ajax({
				url: caller.find('a').attr('href'),
				success: function(data){
					var room_content = $(data).find('.inside').html();
					$room.html( room_content );
					$room.prepend( $close );
					$room.find('[data-propresize="true"]').propResize();
				},
				error: function(){
					$room.remove();
				}
			})
		}
	}
}


$(function(){
	
	$('.nav-tabs a').click(function(){
		var t = $(this).attr('href');
		$(this).parent().addClass('active');
		if( t ){
			$( t ).show();

			$( this ).parent().siblings().each(function( i, e ){
				var k = $( e ).find('a').attr('href');
				if( t == k ) return;
				$( e ).removeClass('active');
				$( k ).hide();
			});
		};
		return false;
	});

	$(".form-group").last().find('button').click(function(){
		if($(".form-group").eq(4).hasClass('hide')){
			$(this).html('LESS FILTERS');
		} else {
			$(this).html('MORE FILTERS');
		}
		$(".form-group").eq(4).toggleClass('hide');
		return false;
	});

	$("#check-children").change(function(){
		$(this).parent().toggleClass( 'hide', ( $(this).prop('checked') ) ).next().toggleClass( 'hide', ( !$(this).prop('checked') ) );
	});

	$("[name=children_count]").change(function(){
		//$(this).toggleClass( 'hide', ( $(this).val() ) );
		$(this).parent().toggleClass( 'hide', ( $(this).val() == "" ) ).prev().toggleClass( 'hide', ( $(this).val() !== "" ) );
		$('#children_alert').toggleClass( 'hide', ( !$(this).val() ) );

		$('[name=child_age_1]').toggleClass( 'hide', ( $(this).val() == "" ) );
		if( $(this).val() == "" ) $('[name=child_age_1]').val('');
		$('[name=child_age_2]').toggleClass( 'hide', ( $(this).val() == "1" ) );
		if( $(this).val() == "1" ) $('[name=child_age_2]').val('');
	});

	$("#check-children-alert").change(function(){
		if( !$(this).prop('checked') ){ 
			$('[name=child_age_1]').attr('disabled','disabled'); 
			$('[name=child_age_2]').attr('disabled','disabled'); 
		}else{ 
			$('[name=child_age_1]').removeAttr('disabled','disabled');
			$('[name=child_age_2]').removeAttr('disabled','disabled');
		}
	});

	$('[name="guests"], [name="beds"]').change(function(){
		$('#more_beds').toggleClass( 'hide', !( $('[name="guests"]').val() == "more" || parseInt( $('[name="beds"]').val() ) > 1 ) );
	});

	/* ==================== SLIDERS */

	var slider = {
		min: 0,
		max: 100
	}

	var barra = $('.bar');
	var subbar = $('.sub_bar');
	var tabs = $('.bar div');
	var currentTab = false;

	tabs.on('mousedown',function(e){
		currentTab = $(this);
		e.stopPropagation();
		return false;
	});

	barra.on('mousemove', function( e ){
		//console.log(e.currentTarget);
		if( e.buttons && e.target == barra[0] ) updateTab( e );
		e.stopPropagation();
		return false;
	});

	barra.on('click', function( e ){
		//console.log(e.currentTarget);
		if( e.target == barra[0] ) updateTab( e );
		e.stopPropagation();
		return false;
	});

	subbar.on('mousemove',function(e){
		//console.log(e.currentTarget);
		if( e.buttons && e.target == subbar[0] ) updateTab( e.offsetX + subbar.position().left );
		e.stopPropagation();
		return false;
	});

	subbar.on('click', function( e ){
		//console.log(e.currentTarget);
		if( e.target == subbar[0] ) updateTab( e.offsetX + subbar.position().left );
		e.stopPropagation();
		return false;
	});

	function updateTab( e, forcePercent ){
		if( currentTab ){
			var larg = barra.width();
			var pos = Math.min( Math.max( 0, ( e.offsetX || e ) - 10 ), barra.width() - 20 );
			var posPer = forcePercent == undefined ? pos/larg : forcePercent;

			//currentTab.html( "$" + Math.floor( ( pos / ( barra.width() - 20 ) ) * 200 ) * 5 );
			currentTab.html( "$" + Math.floor( Math.floor( posPer * 200 ) * 5 ) );
			if( currentTab.hasClass('minimo') ){ slider.min = posPer * 100; $("#price-start").val( Math.floor( posPer * 1000 ) ); }
			if( currentTab.hasClass('maximo') ){ slider.max = posPer * 100; $("#price-end"  ).val( Math.floor( posPer * 1000 ) ); }

			currentTab.css({ left: posPer * 100 + "%" },50);

			subbar.css({'left': Math.min( slider.min, slider.max  )+"%", 'width': Math.max( slider.min, slider.max  ) - Math.min( slider.min, slider.max  ) +"%"});

			lists.filter.price_low =  $("#price-start").val() || 1 ;
			lists.filter.price_high = $("#price-end"  ).val() || 1000;
			lists.filterList();
		}
	}

	currentTab = tabs.eq(0);
	updateTab(0, 0);
	currentTab = tabs.eq(1);
	updateTab(1000, 1);
	currentTab = false;


	/* =============== END SLIDERS */


	/*$(".calendar_price td").click(function(){
		if( !$(this).hasClass("off") ){ $(this).toggleClass("selected"); }
	});*/
	lists.init();
});

var core = {};
core.toggleRightbar = function(show){
	$('html').toggleClass('rightbar', show);
};

var cart = {
	urlUsers: '/topo/client',
	urlSubmit: '/client/submit',
	itens: { rooms:[], offers: [] },
	$elButton: $('.cart'),
	$elTab   : $('.shopping_cart'),
	actualNights: 0,
	init: function(){
		var self = this;

		if( this.$elButton.length < 1 ){ 
			this.$elButton =  $(''+
				'<div class="cart_i cart">'+
				'	<div>'+
				'		<p>$ 0</p>'+
				'		<span>'+
				'			<span></span>'+
				'			<i></i>'+
				'		</span>'+
				'	</div>'+
				'</div>'
			);
			this.$elTab    =  $(''+
				'<div class="shopping_cart close">'+
				'	<div class="shopping_list">'+
				'		<div class="header">'+
				'			<h1>Shopping cart</h1>'+
				'			<i class="close close_shopping"></i>'+
				'		</div>'+
				'		<ul>'+
				'		</ul>'+
				'		<div class="total">'+
				'			<p>TOTAL:</p>'+
				'			<span>$ 0</span>'+
				'		</div>'+
				'		<div class="options">'+
				'			<button class="prev close_shopping">continue shopping</button>'+
				'			<button class="next">check out</button>'+
				'		</div>'+
				'	</div>'+
				'	<div class="special_offers" style="display: none;">'+
				'		<div class="header">'+
				'			<div>'+
				'				<i class="cart"></i>'+
				'				<span>$ 2.225</span>'+
				'				<i class="close close_shopping"></i>	'+	
				'			</div>'+
				'			<h1><span>Check Out</span> / Special Offers</h1>'+
				'		</div>'+
				'		<ul>'+
				'			<li>'+
				/*'				<i class="hide arrow arrow_left"></i>'+
				'				<i class="hide arrow arrow_right"></i>'+
				'				<i class="arrow arrow_left"></i>'+
				'				<i class="arrow arrow_right"></i>'+
				'				<h2>Trail Ride</h2>'+
				'				<h3>FREE (Value $ 200) - Valid for two person</h3>'+
				'				<p>feel the power of nature...ride our horses through a thick tropical jungle to a beautiful hidden beach then gallop with surf breaking at your feet.</p>'+
				'				<button>Watch video</button>'+*/
				'			</li>'+
				'			<li>'+
				'				<i class="hide arrow arrow_left"></i>'+
				'				<i class="hide arrow arrow_right"></i>'+
				/*'				<i class="arrow arrow_left"></i>'+
				'				<i class="arrow arrow_right"></i>'+
				'				<h2>Yoga Lesson</h2>'+
				'				<h3 class="warn">Add 1 night or upgrade room to get this free</h3>'+
				'				<p>feel the power of nature...ride our horses through a thick tropical jungle to a beautiful hidden beach then gallop with surf breaking at your feet.</p>'+
				'				<button>Watch video</button>'+*/
				'			</li>'+
				'			<li>'+
				/*'				<i class="hide arrow arrow_left"></i>'+
				'				<i class="hide arrow arrow_right"></i>'+
				'				<h2>Golf Cart</h2>'+
				'				<h3>$240 - $40 a day</h3>'+
				'				<p>feel the power of nature...ride our horses through a thick tropical jungle to a beautiful hidden beach then gallop with surf breaking at your feet.</p>'+
				'				<button>Watch video</button>'+*/
				'			</li>'+
				'		</ul>'+
				'		<div class="total">'+
				'			<p>TOTAL:</p>'+
				'			<span>$ 0</span>'+
				'		</div>'+
				'		<div class="options">'+
				'			<button class="prev">back</button>'+
				'			<button class="next">next</button>'+
				'		</div>'+
				'	</div>'+
				'	<div class="form_finish" style="display: none;">'+
				'		<div class="header">'+
				'			<div>'+
				'				<i class="cart"></i>'+
				'				<span>$ 2.225</span>'+
				'				<i class="close close_shopping"></i>	'+	
				'			</div>'+
				'			<h1>Finish</h1>'+
				'		</div>'+
				'		<div class="description">'+
				'			<h2>Give us your name and email</h2>'+
				'			<p>Please enter your personal details</p>'+
				'		</div>'+
				'		<form action="#" method="POST">'+
				'			<div class="group">'+
				'				<label for="email">Email</label>'+
				'				<input type="email" class="form-control" id="email" name="email">'+
				'			</div>'+
				'			<div class="just-registered group hide">'+
				'				<p>Thanks! This is email already registered. If you want, we will use storaged infos.</p>'+
				'				<button class="edit">click to ignore storaged infos</button>'+
				'			</div>'+
				'			<div class="group">'+
				'				<div class="col-50">'+
				'					<label for="first_name">First Name</label>'+
				'					<input type="text" class="margin form-control disabled" id="first_name" name="first_name" disabled="disabled">'+
				'				</div>'+
				'				<div class="col-50">'+
				'					<label for="last_name">Last Name</label>'+
				'					<input type="text" class="form-control disabled" id="last_name" name="last_name" disabled="disabled">'+
				'				</div>'+
				'			</div>'+
				'			<div class="group">'+
				'				<div class="col-50">'+
				'					<label for="phone">Phone</label>'+
				'					<input type="text" class="margin form-control disabled" id="phone" name="phone" disabled="disabled">'+
				'				</div>'+
				'				<div class="col-50">'+
				'					<label for="promotion_code">Promotion Code</label>'+
				'					<input type="text" class="form-control disabled" id="promotion_code" name="promotion_code" disabled="disabled">'+
				'				</div>'+
				'			</div>'+
				'			<div class="group">'+
				'				<label for="comments">Comments</label>'+
				'				<textarea class="form-control disabled" id="comments" name="comments" disabled="disabled"></textarea>'+
				'			</div>'+
				'			<div class="group hide">'+
				'				<label for="request">Request</label>'+
				'				<textarea class="form-control" id="request" name="request"></textarea>'+
				'			</div>'+
				'			<div class="group policy">'+
				'				<h1>Cancelation policy</h1>'+
				'				<p>Assures your choice of rooms and provides protection from unforeseen changes in your... plans with flexible cancellation policies.</p>'+
				'				<div class="check">'+
				'					<p>Accept Policy</p>'+				
				'					<input id="policy" type="checkbox" name="policy" value="" class="check-custom hide">'+
				'					<label class="check-custom" for="policy">'+
				'					</label>'+
				'				</div>'+
				'			</div>'+	
				'			<div class="options">'+
				'				<button class="prev">back</button>'+
				'				<button class="next">finish</button>'+
				'			</div>'+
				'		</form>'+
				'	</div>'+
				'	<div class="end_messenger" style="display: none;">'+
				'		<div class="header">'+
				'			<div>'+
				'				<i class="close close_shopping"></i>	'+	
				'			</div>'+
				'			<h1>Dear Mr. Br&ecirc;tas</h1>'+
				'		</div>'+	
				' 		<p class="thanks">Thanks for your choosing to stay with us at Playa Escondida. We are please to confirm your reservation as follows:</p>'+
				'		<div class="group-label">'+
				'			<label>Confirmation Number:</label>'+
				'			<p>123456</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Guest Name:</label>'+
				'			<p>Mr. Pedro Br&ecirc;tas Bastos</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Arrival Date:</label>'+
				'			<p>10/15/05</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Departure Date:</label>'+
				'			<p>10/19/05</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Number of Guests:</label>'+
				'			<p>02</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Accommodations:</label>'+
				'			<p>Ixchel Penthouse Deseo</p>'+
				'		</div>'+
				'		<div class="spacing"></div>'+
				'		<div class="group-label">'+
				'			<label>Rate per Nights:</label>'+
				'			<p>$375</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Special Offer:</label>'+
				'			<p>Trail Ride, Golf Cart</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Add-ons:</label>'+
				'			<p>0</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Check-in Time:</label>'+
				'			<p>15:00 PM</p>'+
				'		</div>'+
				'		<div class="group-label">'+
				'			<label>Check-out Time:</label>'+
				'			<p>11:00 AM</p>'+
				'		</div>'+
				'		<div class="total">'+
				'			<p>TOTAL:</p>'+
				'			<span>$ 0</span>'+
				'		</div>'+
				' 		<p class="total">We will send you an email shortly. In mean time you can shop for more unforgettable experiences in our online store.</p>'+
				' 		<a href="shop.html" class="button">add-ons</a>'+
				'	</div>'+
				'</div>');
			this.$elOverlay = $('<div class="shopping_cart_overlay close"></div>');

			$('.inside').prepend( this.$elButton );
			$('.inside').prepend( this.$elOverlay );
			$('.inside').prepend( this.$elTab );


			this.$elTab.playaMenu();

			this.$elTab.addClass('close');
		};

		// libs faltando

		//<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/jquery.effects.core.js"></script>
		//<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/jquery.effects.slide.js"></script>

		$('.close_shopping, .shopping_cart_overlay').click(function(e){
			self.$elTab.addClass('close');
			self.$elOverlay.addClass('close');
			e.stopPropagation();
			return false;
			//$('.shopping_cart').hide('slide', {direction: 'right'}, 300);// só depois de adicionar as libs
		});

		$('.cart, .shopping_cart h1, .shopping_cart .header div, .special_offers .prev').click(function(e){			
			self.$elTab.removeClass('close');
			self.$elOverlay.removeClass('close');
			$('.shopping_list').show();
			$('.special_offers').hide();
			$('.form_finish').hide();	
			$('.end_messenger').hide();
			$(window).resize();

			e.stopPropagation();
			return false;					
			//$('.shopping_cart').show('slide', {direction: 'right'}, 300); // só depois de adicionar as libs
		});
		$('.shopping_list .next, .form_finish .prev').click(function(e){
		/* BLOCO COMENTADO PARA PULAR OFERTAS */
		/*  $('.shopping_list').hide();
			$('.special_offers').show();
			$('.form_finish').hide();	
			$('.end_messenger').hide();	
			$(window).resize();			
			
			e.stopPropagation();
			return false;					
		});
		$('.special_offers .next').click(function(e){*/
			$('.shopping_list').hide();
			$('.special_offers').hide();
			$('.form_finish').show();
			$('.end_messenger').hide();	
			$(window).resize();				
			
			e.stopPropagation();
			return false;		
		});

		this.$elTab.find('[name=email]').change(function(){
			self.checkEmail( $(this).val() );
		});

		this.$elTab.find('.just-registered button').click(function(e){
			self.$elTab.find('.form_finish .disabled').removeClass('disabled').removeAttr('disabled');
			self.$elTab.find('.just-registered').addClass('hide');

			e.stopPropagation();
			return false;
		});

		this.$elTab.find('.form_finish .next').click(function(e){
			self.submit();	
			e.stopPropagation();
			return false;
		});

		var memRooms = this.mem();
		if( memRooms ){
			this.itens = memRooms;
			this.updateOffers();
			this.updateLists();
			this.$elTab.find('[name=request]').val(JSON.stringify(this.itens));
		}else{
			this.updateOffers();
		}	
	},
	checkEmail: function( email ){
		var self = this;

		$.ajax({
			url: this.urlUsers,
			data: { email: email },
			dataType: "json",
			success: function( res ){
				if( res.result == "success" ){
					self.$elTab.find('.just-registered').removeClass('hide');
					self.$elTab.find('.form_finish input[type=text], .form_finish textarea').addClass('disabled').attr('disabled','disabled');
				}else{
					self.$elTab.find('.form_finish input[type=text], .form_finish textarea').removeClass('disabled').removeAttr('disabled','disabled');
					self.$elTab.find('.just-registered').addClass('hide');
				}
			}
		});
	},
	show: function(){
		$('.shopping_cart').removeClass('close');
		$('.shopping_list').show();

		$('.special_offers').hide();
		$('.form_finish').hide();	
		$('.end_messenger').hide();
		$(window).resize();
	},
	addRoom: function( room ){
		var self = this;

		( room || (
			room = {
				img:   "http://projetos.thbastos.com/playa/content/price/thumb_01.png",
				name:  "IXCHEL PENTHOUSE",
				nights: 4,
				value:  1125
			}
		));

		room.type = 'room';

		this.itens.rooms.push( room );

		this.$elTab.find('[name=request]').val(JSON.stringify(this.itens));
		this.mem( this.itens );
		this.updateLists();
		this.show();
	},
	hasRoom : function( room ){
		var index = -1;
		for (var i = this.itens.rooms.length - 1; i >= 0; i--) {
			if (this.itens.rooms[i] == room ){
				index = i;
				break;
			}
		};
		return index;
	},
	removeRoom : function( room ){
		var index = -1;
		index = this.hasRoom( room );

		if (index > -1) {
			this.itens.rooms.splice(index, 1);
		}else{
			index = this.hasItem( room );

			if (index > -1) {
				this.itens.offers.splice(index, 1);
			};
		}

		if (index > -1) this.mem( this.itens );

		if (index > -1) this.updateLists();
	},
	addItem : function( item ){
		var self = this;

		( item || (
			item = {
				name:  "TRIAL RIDE",
				count: 1,
				value: 1125
			}
		));

		item.type = 'item';
		this.itens.offers.push( item );

		this.$elTab.find('[name=request]').val(JSON.stringify(this.itens));		
		this.mem( this.itens );
		this.updateLists();
		//this.show();
	},
	hasItem : function( item ){
		var index = -1;
		for (var j = this.itens.offers.length - 1; j >= 0; j--) {
			if (this.itens.offers[j].name == item.name ){
				index = j;
				break;
			}
		};
		return index;
	},
	reset: function(){
		this.itens.rooms = [];
		this.itens.offer = [];
		this.mem(this.itens);
		this.updateLists();
	},
	updateOffers: function( list ){
		var self = this;

		var list = [{
			id: 0,
			name:"Trial ride",
			value: 0,
			desc_cond:"Add 1 night or upgrade room to get this free",
			desc_value:"FREE (Value $200) - Valid for tow person",
			desc:"feel the power of nature...ride our horses through a thick tropical jungle to a beautiful hidden beach then gallop with surf breaking at your feet.",
			check: null,
			videos: [],
			images: [],
			step: 0
		},{
			id: 1,
			name:"Yoga lesson",
			value: 0,
			desc_cond:"Add 1 night or upgrade room to get this free",
			desc_value:"FREE (Value $200) - Valid for tow person",
			desc:"feel the power of nature...ride our horses through a thick tropical jungle to a beautiful hidden beach then gallop with surf breaking at your feet.",
			check: null,
			videos: [],
			images: [],
			step: 1
		},{
			id: 2,
			name:"Yoga lesson 2",
			value: 0,
			desc_cond:"Add 1 night or upgrade room to get this free",
			desc_value:"FREE (Value $200) - Valid for tow person",
			desc:"feel the power of nature...ride our horses through a thick tropical jungle to a beautiful hidden beach then gallop with surf breaking at your feet.",
			check: null,
			videos: [],
			images: [],
			step: 1
		},{
			id: 3,
			name:"Golf cart",
			value: 40,
			desc_cond:"Add 1 night",
			desc_value:"Add 1 night",
			desc:"feel the power of nature...ride our horses through a thick tropical jungle to a beautiful hidden beach then gallop with surf breaking at your feet.",
			check: null,
			videos: [],
			images: [],
			step: 2
		}];

		//list = list.concat((list.reverse()));

		this.$elTab.find('.special_offers .arrow_left').click(function(){
			$(this).parent().append( $(this).siblings('div').first() );
			$(this).siblings('div').not('.hide').addClass('hide');
			$(this).siblings('div').first().removeClass('hide');
		});

		this.$elTab.find('.special_offers .arrow_right').click(function(){
			$(this).parent().prepend( $(this).siblings('div').last() );
			$(this).siblings('div').not('.hide').addClass('hide');
			$(this).siblings('div').first().removeClass('hide');
		});


		this.$elTab.find('.special_offers ul li > div').remove();
		var offersTemplate = $('<div>'+
					'<h2>Yoga Lesson</h2>'+
					'<input type="checkbox" name="check_offer" value="">'+
					'<h3 class="warn">Add 1 night or upgrade room to get this free</h3>'+
					'<p>feel the power of nature...ride our horses through a thick tropical jungle to a beautiful hidden beach then gallop with surf breaking at your feet.</p>'+
					'<button class="videos">Watch videos</button>'+
					'<button class="images">View images</button>'+
					//'<button name="add">Add to cart</button>'+
				'</div>');

		for (var i = 0; i < list.length; i++) {
			var offer = offersTemplate.clone();
			offer.find('h2').html(list[i].name);
			offer.find('h3').html(list[i].desc_value);				
			offer.find('p').html(list[i].desc);				
			//if( !list[i].videos || !list[i].videos.length )  offer.find('.videos').hide();
			if( !list[i].images || !list[i].images.length )  offer.find('.images').hide();
			offer.data('offer', list[i]);

			offer.find('button[name=add]').click(function(){
				self.addItem( $(this).parent().data('offer') );
			});

			offer.find('[name="check_offer"]').change(function(){	
				if( !$(this).prop('checked') ){ 
					self.removeRoom( $(this).parent().data('offer') );
				}else{
					self.addItem( $(this).parent().data('offer') );
					$(this).parent().siblings().find('[name="check_offer"]:checked').not($(this)).each(function(){
						$(this).prop('checked', false);
						self.removeRoom( $(this).parent().data('offer') );
					});				
				} 		
				//self.addItem( $(this).parent().data('offer') );
			});

			if( i > 2 ) offer.addClass('hide');
			self.$elTab.find('.special_offers ul li').eq( list[i].step ).append( offer );
		};


		self.$elTab.find('.special_offers ul li').each(function( i, e ){
			if( $(this).children('div').length > 1 ){
				$(this).children().not('div').removeClass('hide');
			}
		});
	},
	updateLists : function(){
		var self = this;
		var totalPrices = 0;

		this.$elTab.find('.shopping_list ul').empty();
		this.actualNights = 0;

		for (var i = this.itens.rooms.length - 1; i >= 0; i--) {

			var room = this.itens.rooms[i];
			var $elRoom = $(''+
					'<li>'+
					'	<img src="http://projetos.thbastos.com/playa/content/price/thumb_01.png">'+
					'	<h2>IXCHEL PENTHOUSE</h2>'+
					'	<p class="guests"></p>'+
					'	<p class="desc"></p>'+
					'	<p class="value"></p>'+
					'	<i class="close"></i>'+
					'</li>'
				);	
			$elRoom.find('img').attr('src', room.img);
			$elRoom.find('h2').html( room.name );
			$elRoom.find('.guests').html( (room.guests?room.guests:1) + ' Guest' + ( room.guests > 1 ?'s':'' ) + ( room.children.length > 0 ? ( ' / ' + ( room.children.length == 1? "1 child" : room.children.length + " children" )  ) : '' ));
			$elRoom.find('.desc').html( room.nights + ' Night' + ( room.nights > 1 ?'s':'' ) );
			$elRoom.find('.value').html( self._formatValue( room.value ) );
			$elRoom.data('room', room)
			$elRoom.find('.close').click(function(){
				self.removeRoom( $(this).parent().data('room') );
			});

			this.actualNights += room.nights;
			totalPrices += room.value;

			this.$elTab.find('.shopping_list ul').append( $elRoom );
		};

		/*for (var i = this.itens.offers.length - 1; i >= 0; i--) {
			var item = this.itens.offers[i];
			var $elRoom = $(''+
					'<li>'+
					'	<h2>IXCHEL PENTHOUSE</h2>'+
					'	<p class="value">$ 1.1125</p>'+
					'	<i class="close"></i>'+
					'</li>'
				);	
			$elRoom.find('h2').html( item.name );
			$elRoom.find('.value').html( "$ " + item.value * this.actualNights );
			$elRoom.data('item', item)
			$elRoom.find('.close').click(function(){
				self.removeRoom( $(this).parent().data('item') );
			});


			totalPrices += item.value * this.actualNights;

			this.$elTab.find('.shopping_list ul').append( $elRoom );
		};*/

		this.$elButton.find('span span').html( this.itens.rooms.length );
		this.$elButton.find('p').html( self._formatValue( totalPrices ) );

		this.$elTab.find('.header div span').html( self._formatValue( totalPrices ) );

		this.$elTab.find('.total span').html( self._formatValue( totalPrices ) );

		/* OFFERS */
		self.$elTab.find('.special_offers ul li > div').addClass('disable');
		/* 1o Level */ 
		self.$elTab.find('.special_offers ul li:eq(0) > div').each(function(){
			if( self.actualNights > 3 ){
				$(this).toggleClass('disable',false);
				$(this).find('input').removeAttr('disabled');
				if( self.hasItem( $(this).data('offer') ) > -1 ){ $(this).find('input').prop('checked',true); }
				$(this).find('h3').html( $(this).data('offer').desc_value );
			}else{
				self.removeRoom( $(this).data('offer') );
				$(this).find('input').prop('checked',false);
				$(this).toggleClass('disable',true);
				$(this).find('input').attr('disabled','disabled');
				$(this).find('h3').html( $(this).data('offer').desc_cond );
			}
		});	
		/* 2o Level */ 
		self.$elTab.find('.special_offers ul li:eq(1) > div').each(function(){
			if( self.actualNights > 4 ){
				$(this).toggleClass('disable',false);
				$(this).find('input').removeAttr('disabled');
				if( self.hasItem( $(this).data('offer') ) > -1 ){ $(this).find('input').prop('checked',true); }
				$(this).find('h3').html( $(this).data('offer').desc_value );
			}else{
				self.removeRoom( $(this).data('offer') );
				$(this).find('input').prop('checked',false);
				$(this).toggleClass('disable',true);
				$(this).find('input').attr('disabled','disabled');
				$(this).find('h3').html( $(this).data('offer').desc_cond );
			}
		});	
		/* 3o Level */ 
		self.$elTab.find('.special_offers ul li:eq(2) > div').each(function(){
			if( self.actualNights > 0 ){
				$(this).toggleClass('disable',false);
				$(this).find('input').removeAttr('disabled');
				if( self.hasItem( $(this).data('offer') ) > -1 ){ $(this).find('input').prop('checked',true); }
				$(this).find('h3').html( "$ " + ( self.actualNights * $(this).data('offer').value ) + " - $ " + $(this).data('offer').value + " A DAY" );
			}else{
				self.removeRoom( $(this).data('offer') );
				$(this).find('input').prop('checked',false);
				$(this).toggleClass('disable',true);
				$(this).find('input').attr('disabled','disabled');
				$(this).find('h3').html( $(this).data('offer').desc_cond );
			}
		});
	},
	mem : function( data ){
		if( data ){
			if( localStorage ){
				localStorage.setItem('itens', JSON.stringify(data));
			}else{
				document.cookie="itens="+JSON.stringify(data);
			}
		}else{
			if( localStorage ){
				data = localStorage.getItem('itens');				
			}else{
				data = false;
				var ca = document.cookie.split(';');
			    for(var i=0; i<ca.length; i++) {
			        var c = ca[i];
			        while (c.charAt(0)==' ') c = c.substring(1);
			        if (c.indexOf(name) == 0){ data = c.substring(name.length,c.length); break; }
			    }
			}
			if( data ) data = JSON.parse(data);
		};

		return data;
	},
	submit: function(){
		var self = this;
		
		var send = {
			email: this.$elTab.find('[name=email]').val(),
			first_name: this.$elTab.find('[name=first_name]').val(),
			last_name: this.$elTab.find('[name=last_name]').val(),
			phone: this.$elTab.find('[name=phone]').val(),
			promotion_code: this.$elTab.find('[name=promotion_code]').val(),
			comments: this.$elTab.find('[name=comments]').val(),
			data: this.itens,
		};

		var valid = true;

		if( !send.email || !(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i).test(send.email) ) { valid = false; }
		this.$elTab.find('[name=email]').toggleClass('invalid', ( !send.email || !(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i).test(send.email) ) );
 

		if( !this.$elTab.find('[name="policy"]').prop('checked') ){ valid = false; }
		this.$elTab.find('[name="policy"]').toggleClass('invalid', !this.$elTab.find('[name="policy"]').prop('checked') );
	
		if( send.data.length == 0 ){ valid = false; }

		if( !valid ){ return; }

		$.ajax({
			url: self.urlSubmit,
			data: {send:send},
			method: "POST",
			dataType: "json",
			success:function( res ){
				self.showResult( res.data );
				//self.reset();
			}
		})

		console.log( send );
	},
	showResult: function(res){
		$('.shopping_list').hide();
		$('.special_offers').hide();
		$('.form_finish').hide();
		$('.end_messenger').show();	

		$('.end_messenger h1').html('Dear ' + res.last_name);
		$('.end_messenger .group-label:eq(0) p').html(res.confirmNumber);
		$('.end_messenger .group-label:eq(1) p').html(res.name);
		$('.end_messenger .group-label:eq(2) p').html( new Date( parseFloat(res.date_arrival)).toLocaleDateString() );
		$('.end_messenger .group-label:eq(3) p').html( new Date( parseFloat(res.date_departure)).toLocaleDateString() );
		$('.end_messenger .group-label:eq(4) p').html( res.guests );
		$('.end_messenger .group-label:eq(5) p').html( res.rooms.join('<br>') );
		$('.end_messenger .group-label:eq(6) p').html( this._formatValue( Math.ceil( res.price / res.nights ) ) );
		$('.end_messenger .group-label:eq(7) p').html( res.offers.join('<br>') );
		$('.end_messenger .group-label:eq(8) p').html( '' );

		$(window).resize();
	},
	_formatValue: function(value){
		var r ;
		( value && ( r = value.toString().split('').reverse() ) || ( r = ['0'] ) );
		for (var i = r.length - 1; i >= 0; i--) {
			if( i > 0 && ( i == 3 || i % 3 == 0 ) ) r.splice(i,0,'.');
		};
		return '$ ' + r.reverse().join('');
	}
};

$(function(){


	$('.mobile-menu').click(function(){
		$('#navigation').toggleClass('open');
	});

	$('a').each(function(){
		if( !$(this).attr('href') || !window.location.href ){ return; }
		var href = $(this).attr('href').split('/').pop().split('?').shift();
		var local = window.location.href.split('/').pop().split('?').shift();
		console.log( href, local );
		if( href == local ) $(this).css('font-weight','bold');
	});

	$('.tabs li h2').click(function(){
		//$(this).parent().toggleClass('open');
		$(this).siblings().slideToggle();
		var t = $(this).offset().top;
		$('html, body').animate({scrollTop: t }, 300 );
	});


	cart.init();

	// Open room html in prices page
	/*
	$("div#price ul li span a").click(function() {
		alert('oi');
		var url = $(this).attr('href');
	    $.get(url,function(data){
	        $("div.inside").append(data);
	    });
	});
	*/

});


$(function () {
	$('.contact_form form').submit(function(e){
		var valid = true;

		var emailVal = !$('.contact_form form [name=email]').val() || !(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i).test($('.contact_form form [name=email]').val());
		if( emailVal ) { valid = false; }
		$('.contact_form form [name=email]').toggleClass('invalid', emailVal );

		var contentVal = !$('.contact_form form [name=comments]').val();
		if( contentVal ) { valid = false; }
		$('.contact_form form [name=comments]').toggleClass('invalid', contentVal );

		if( $('.contact_form button').hasClass('disabled') || !$('.thanks').hasClass('hide') ) valid = false; 

		if( valid ){
			$.ajax(
			{
				url:"/topo/client/contact",
				data: $('.contact_form form').serialize(),
				method: "POST",
				beforeSend: function(){
					$('.contact_form button').html('sending...');
					$('.contact_form button, .contact_form input, .contact_form textarea').attr('disabled','disabled');
					$('.contact_form button, .contact_form input, .contact_form textarea').addClass('disabled');
				},
				success: function(e){
					//e = JSON.parse(e);
					if( e.result && e.result == "success"  ){
						$('.contact_form button').html('done!');
						setTimeout(function(){ 
							$('.contact_form button').removeClass('disabled').removeAttr('disabled').html('send another');
						    //$('.contact_form button, .contact_form input').removeAttr('disabled');
						    //$('.contact_form button, .contact_form input').removeClass('disabled'); 
						}, 2000);

						if( typeof ga !== "undefined" ){
							ga('send', 'pageview', {
							  'page': '/contact-form/success',
							  'title': 'Contact Form - Success'
							});
						}
						$('.thanks').removeClass('hide');

					}else{
						$('.contact_form button').html('fail... let`s try again?');					
						setTimeout(function(){ 
							$('.contact_form button').html('send');
						    $('.contact_form button, .contact_form input, .contact_form textarea').removeAttr('disabled');
						    $('.contact_form button, .contact_form input, .contact_form textarea').removeClass('disabled');
						}, 2000);		

						if(ga){
							ga('send', 'pageview', {
							  'page': '/contact-form/error',
							  'title': 'Contact Form - Error'
							});
						}

					}
					
				},
				error: function(){
					$('.contact_form button').html('fail... let`s try again?');
					setTimeout(function(){ $('.contact_form button').html('send'); }, 2000);
				}
			});

		}

		if( !$('.thanks').hasClass('hide') ){
			$('.thanks').addClass('hide');
			$('.contact_form button, .contact_form input, .contact_form textarea').removeClass('disabled').removeAttr('disabled');
			$('.contact_form input, .contact_form textarea').val('').html('');
			$('.contact_form button').html('send');
		}
		return false;
	});
})
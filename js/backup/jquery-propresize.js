


;(function ( $, window, document, undefined ) {

    


    var pluginName = 'propResize',

        defaults = {

            refElem: false

        };



    // The actual plugin constructor

    function Plugin( element, options ) {

        this.element = element;

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;

        this._name = pluginName;
     
        this.init();

    }



    Plugin.prototype.init = function () {


        var self = this;

        $(this.element).each(function(){

            var dataHeight = $(this).data('height');
            var dataWidth = $(this).data('width');

            var target = {
                elem : $(this),
                height : dataHeight? $.isNumeric(dataHeight) ? dataHeight : dataHeight.split('x')[0] : $(this).height(),
                heightMulti: dataHeight? $.isNumeric(dataHeight) ? 1 : dataHeight.split('x')[1] : 1 ,
                width  : dataWidth? $.isNumeric(dataWidth) ? dataWidth : dataWidth.split('x')[0] : $(this).width() ,
                widthMulti: dataWidth? $.isNumeric(dataWidth) ? 1 : dataWidth.split('x')[1] : 1 ,
            };

            target.propQuad = $(this).data('prop')?$(this).data('prop'):( ( target.height ) / ( target.width ) );
            target.propRect = target.heightMulti / target.widthMulti ;
            target.class = $(this).attr('class');

            target.elem.data('propresize', target );
            target.width = 0; // restart width to allow auto-start

        });


        $(window).resize(function(){
            if(self.options.refElem) self.resize( self.options.refElem.data('propresize') );
            $(self.element).each(function(){
                self.resize($(this).data('propresize'));
            });
        }).resize();

    };


    Plugin.prototype.resize = function( target, force ){

        var self = this;
        var refTarget = self.options.refElem && self.options.refElem.data('propresize') !== target ? self.options.refElem.data('propresize') : target ;

        if( target.elem.width() !== target.width || force ){          

            target.width = target.elem.width();
            var refWidth = Math.floor( refTarget.elem.width() * target.propQuad );
            var refPropo = self.options.refElem ? target.heightMulti : target.propRect ;

            target.height = refWidth * ( refPropo ) ;
            target.elem.height( target.height );
            target.elem.trigger( 'resized', [target] );

        }
        
    };


    // A really lightweight plugin wrapper around the constructor, 

    // preventing against multiple instantiations

    $.fn[pluginName] = function ( options ) {

        /*return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, */
                new Plugin( this, options );// );
        /*    }
        });*/

        return this;

    }



})( jQuery, window, document );
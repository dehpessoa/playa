


;(function ( $, window, document, undefined ) {

    


    var pluginName = 'playaMenu',

        defaults = {
            propertyName: "value"
        };



    // The actual plugin constructor

    function Plugin( element, options ) {

        this.element = element;
        this.options = $.extend( {}, defaults, options);

        this.anchor = 0;

        this._defaults = defaults;
        this._name = pluginName;

        this.menu = $(this.element);
        this.init();
    }



    Plugin.prototype.init = function () {


    	var self = this;
    	//this.menu.css('position','fixed');
        this.update();

        $(window).on('scroll',function(){
	    	var wh = $(window).height();
	    	var dh = $(document).height();
	    	var mh = self.menu.outerHeight();    
            var ws = $( window ).scrollTop();

            var ms = ( self.anchor >= ws || mh <= wh ? 0 : ( ws - self.anchor < mh - wh )? - ws + self.anchor :  wh - mh );
            self.menu.css('top', ms );
        	//self.menu.css('top',( wh > mh ? 0 : -( wh - mh ) * ( $( window ).scrollTop() / ( wh - dh ) ) ));
            if( wh > mh ){ $('body').removeClass('menuFixed'); }else{ $('body').addClass('menuFixed'); }
        });

        $(window).scroll();
        $(window).resize( function(){ $(window).scroll(); });
    };

    Plugin.prototype.update = function(){
        this.anchor = $( window ).scrollTop() + this.menu.outerHeight() <= $(document).height()? $( window ).scrollTop() : $(document).height() - this.menu.outerHeight() ;
    };

    // A really lightweight plugin wrapper around the constructor, 

    // preventing against multiple instantiations

    $.fn[pluginName] = function ( options ) {

        return this.each(function () {

            if (!$.data(this, 'plugin_' + pluginName)) {

                $.data(this, 'plugin_' + pluginName, 
                new Plugin( this, options ));

            }else{
                $.data(this, 'plugin_' + pluginName, 
                new Plugin( this, options )).update( options );
            }

        });

    }



})( jQuery, window, document );
var t = {
	lib: {},
	forgot: {},
	/* 
	*  PUSH A NEW TERM AT LIB
	*  index - tag to index the new term
	*  value - fulltext that will be returned	
	*
	*/
	p: function ( index, value ) {
		this.lib[ index.toLowerCase().replace(/[^0-9a-zA-Z]/gi,"_") ] = value;
	},
	/* 
	*  RETURN THE TERM IF THIS EXIST
	*  if the term was not declared, this will return the key and store this request at 'forgot' object to debugs	
	*
	*/
	g: function( index ){
		if( !this.lib[ index.toLowerCase().replace(/[^0-9a-zA-Z]/gi,"_") ] ) this.forgot[ index.toLowerCase().replace(/[^0-9a-zA-Z]/gi,"_") ] = index;
		return ( this.lib[ index.toLowerCase().replace(/[^0-9a-zA-Z]/gi,"_") ] || index );
	}
};

t.p('123456', 						"123456");
t.p('accept_policy', 				"Accept Policy");
t.p('avaliable_days', 				"Avaliable days");
t.p('accommodations', 				"Accommodations");
t.p('add_ons', 						"Add-ons");
t.p('arrival_date', 				"Arrival Date");
t.p('assures_your_choice_of_rooms', "Assures your choice of rooms and provides protection from unforeseen changes in your... plans with flexible cancellation policies.");
t.p('back', 						"back");
t.p('cancelation_policy', 			"Cancelation policy");
t.p('check_in_time', 				"Check-in Time");
t.p('check_out_time', 				"Check-out Time");
t.p('check_out',					"check out");
t.p('click_to_use_storaged_infos', 	"click to use storaged infos");
t.p('comments', 					"Comments");
t.p('confirmation_number', 			"Confirmation Number");
t.p('continue_shopping', 			"continue shopping");
t.p('departure_date',			 	"Departure Date");
t.p('done_',				 		"done!");
t.p('email', 						"Email");
t.p('fail___let_s_try_again_',		'fail... let`s try again?');
t.p('filters', 						"FILTERS");
t.p('finish', 						"Finish");
t.p('first_name', 					"First Name");
t.p('give_us_your_name_and_email', 	"Give us your name and email");
t.p('guest_name', 					"Guest Name");
t.p('hide_filters', 				"HIDE FILTERS");
t.p('last_name', 					"Last Name");
t.p('loading___', 					"Loading...");
t.p('may_to_oct', 					"May to oct");
t.p('next', 						"next");
t.p('nov_to_aprill', 				"Nov to Aprill");
t.p('number_of_guests', 			"Number of Guests");
t.p('phone', 						"Phone");
t.p('enter_personal_details', 		"Please enter your personal details");
t.p('select_your_check__br_in_date',"PLEASE SELECT YOUR CHECK IN <br>DATE AND NUMBER OF GUESTS");
t.p('select_your_check_in_date',	"PLEASE SELECT YOUR CHECK IN DATE AND NUMBER OF GUESTS");
t.p('promotion_code',				"Promotion Code");
t.p('rate_per_nights',				"Rate per Nights");
t.p('request',						"Request");
t.p('send',							"send");
t.p('send_another',					'send another');
t.p('sending___',					"sending...");
t.p('shopping_cart',				"Shopping cart");
t.p('special_offer',				"Special Offer");
t.p('special_offers',				"Special Offers");
t.p('email_already_registered',		"Thanks! This is email already registered. If you want, we will use storaged infos.");
t.p('thanks_for_stay_with_us',		"Thanks for your choosing to stay with us at Playa Escondida. We are please to confirm your reservation as follows");
t.p('special_offer',				"Special Offer");
t.p('we_will_send_you_an_email',	"We will send you an email shortly. In mean time you can shop for more unforgettable experiences in our online store.");

var config = {
	serverUrl : 'http://projetos.entreoutros.com/playa/topo/',
}
var weather = {
	getData: function(){
		var self = this;
		$.ajax({
			url: "http://www.playa-escondida.com/topo/weather",
			data: {url: 'http://sayulitabeach.com/weather/weather.json' },
			dataType: 'json',
			method: "POST",
			cache: false,
			beforeSend: function(){
				self.updateView( 'before' );
			},
			success: function( xml ){
				self.updateView( 'success', xml.data );
			},
			error: function( data ){
				self.updateView( 'error', data );
			}
		})
	},
	updateView: function( state, data ){

		var view = $('.weather');


		switch( state ){
			case 'before':
				view.addClass('loading');
				//view.html( 'loading' );
			break;
			case 'error':
				view.addClass('hide');
			break;
			default:
				var date = new Date(data.site.time);
				var weekday = new Array(7);
				weekday[0]=  "Sunday";
				weekday[1] = "Monday";
				weekday[2] = "Tuesday";
				weekday[3] = "Wednesday";
				weekday[4] = "Thursday";
				weekday[5] = "Friday";
				weekday[6] = "Saturday";

				view.find('.date').html( date.toLocaleDateString() );
				view.find('.day').html( weekday[date.getDay()].toUpperCase() );

				view.removeClass('loading');
				view.addClass( data.site.properties.forecast.values[0].value.toString().toLowerCase().replace(/[^a-z0-9]/,'-') );
				var prev = '' +
					data.site.properties.forecast.values[0].value.toString().toUpperCase() + ": "+ 
					data.site.properties.outdoorTemperature.values[0].value.toString().toUpperCase() + " &deg;F / "+
					data.site.properties.outdoorTemperature.values[1].value.toString().toUpperCase() + " &deg;C";
				view.find('.prevision').html( prev );
				var prev = 'HUMIDITY: ' + data.site.properties.outdoorHumidity.values[0].value.toString().toUpperCase() +"% " +
					"WIND: " + data.site.properties.windSpeed.values[0].value.toString().toUpperCase() + ' PH';
				view.find('.data').html( prev );
				// Forecast
				//
		}
	}
};